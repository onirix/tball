﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {
	public Color color;

	// Use this for initialization
	void Start () {
		Renderer rend = GetComponent<Renderer>();
		rend.material.SetColor("_Color", color);
	}
}
