﻿using UnityEngine;
using System.Collections;

public class IWasSelectedClass: MonoBehaviour {
	public delegate void IWasSelected(GameObject  isme);
	public static IWasSelected iWasSelected;

	public bool selected = false;

	void OnEnable(){
		IWasSelectedClass.iWasSelected += IsMe;
	}
	void OnDisable(){
		IWasSelectedClass.iWasSelected -= IsMe;
	}
	void IsMe(GameObject isme){
		if (isme != gameObject){
			selected = false;
		}
	}
}
