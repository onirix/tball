﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class UserInfo
{
	public string appid;
	public string srml;
	public string srnck;
	public int wins;
	public int loses;
	public int golsout;
	public int golsin;
	public float exp;
	public int level;
	public Team team;
	public int istadium;
	public int registered;
	public int history;
	public int coins;
	public List<PowerUpsStored> powerups;
	public List<string> teams;
	public PlayerPosition[] positions;

	public UserInfo ()
	{
		appid = "";
		srml = "";
		srnck = "";
		istadium = 0;
		registered = 0;
		wins = 0;
		loses = 0;
		golsin = 0;
		golsout = 0;
		level = 0;
		exp = 0;
		history = 0;
		coins = 0;
		powerups = new List<PowerUpsStored>();
		teams = new List<string>();
	}
}

[Serializable]
public class PlayerPosition
{
	public float x;
	public float y;
	public float z;

	public PlayerPosition (float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}

[Serializable]
public class PowerUpsStored
{
	public string name;
	public int count;

	public PowerUpsStored(string name, int count){
		this.name = name;
		this.count = count;
	}
}

[Serializable]
public class Configuration
{
	public bool powerUps;

	public Configuration (bool powerUps)
	{
		this.powerUps = powerUps;
	}
}

[Serializable]
public class Team
{
	public string name;
	public int ipower;

	public Team (string name, int powerup)
	{
		this.name = name;
		this.ipower = powerup;
	}
}
