﻿using UnityEngine;
using System.Collections;

public class BehaviourStoreTeamPortrait : BehaviourStore {

	void OnMouseDown(){
		if (!StoreManager.s.mainPanel.activeInHierarchy){
			AddListener();
			Display();
			StoreManager.s.SetTeamsStored();
		}
	}

	public override void AddListener(){
		back.onClick.RemoveAllListeners();
		back.onClick.AddListener(() => StoreManager.s.HideTeamPortraitPanel());
	}

}
