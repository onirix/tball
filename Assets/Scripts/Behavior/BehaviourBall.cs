﻿using UnityEngine;
using System.Collections;

public class BehaviourBall : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x > 19.5f || transform.position.x < -19.5f || transform.position.z > 9.5f || transform.position.z < -9.5f){
			Rigidbody rig = GetComponent<Rigidbody>();
			rig.velocity = Vector3.zero;
			rig.angularVelocity = Vector3.zero;
			transform.position = Vector3.zero;
		}
	}
}
