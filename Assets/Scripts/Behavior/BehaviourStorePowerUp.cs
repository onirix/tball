﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviourStorePowerUp : BehaviourStore
{
	void Start ()
	{
		item = StoreManager.s.powerupsPanel.GetComponentInChildren<Button> ();
	}

	void OnMouseDown ()
	{
		if (!StoreManager.s.mainPanel.activeInHierarchy) {
			AddListener ();
			Display ();
			StoreManager.s.SetPowerUpsPanel ();
		}
	}

	public override void AddListener ()
	{
		back.onClick.RemoveAllListeners ();
		back.onClick.AddListener (() => StoreManager.s.HidePowerUpsPanels ());
	}
}
