﻿using UnityEngine;
using System.Collections;
using System;

public class BehaviorTeamWait : MonoBehaviour
{


	void OnTriggerStay (Collider other)
	{
		if (other.CompareTag ("Team")) {
			other.name = name + "team";
			other.transform.rotation = transform.rotation;
			other.transform.parent = transform;
			//NetworkTransformChild playerChild = player.AddComponent<NetworkTransformChild> ();
			//playerChild.target = playerTeam.transform;
			other.transform.localScale = new Vector3 (1.5f, 2.0f, 1.5f);
			ITeam team = other.GetComponent (typeof(ITeam)) as ITeam;
			GetComponent<BehaviorPlayer> ().SetTeam (team);

			try {
				Destroy (other.GetComponent<OnPlayerSelected> ());
				Destroy (this);
			} catch (NullReferenceException ex) {
			}
		}
	}
}
