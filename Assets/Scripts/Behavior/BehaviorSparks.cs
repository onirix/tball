﻿using UnityEngine;
using System.Collections;

public class BehaviorSparks : MonoBehaviour
{

	float timer;
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		if (timer > 5) {
			GetComponent<EllipsoidParticleEmitter> ().maxEmission = 0;
			Destroy (gameObject, 10.0f);
		}
	}
}
