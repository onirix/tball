﻿using UnityEngine;
using System.Collections;

public class BehaviourStorePowerUpPortrait : BehaviourStore
{
	void OnMouseDown ()
	{
		if (!StoreManager.s.mainPanel.activeInHierarchy) {
			AddListener ();
			Display ();
			StoreManager.s.SetPowerUpsStored ();
		}
	}

	public override void AddListener ()
	{
		back.onClick.RemoveAllListeners ();
		back.onClick.AddListener (() => StoreManager.s.HidePowerUpPortraitPanel ());
	}
}
