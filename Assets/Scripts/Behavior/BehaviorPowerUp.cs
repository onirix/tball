﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviorPowerUp : MonoBehaviour
{
	/*public delegate void IsFor(int forall);
	public static event IsFor isFor;*/

	public GameObject sparks;
	//private AudioSource audioTake;
	public int count;
	public int index;

	//Animator powerUpMessage;

	public IPowerUp powerup;

	public Text cointText;

	//bool lockisfor = false;

	void Awake ()
	{
		//powerup = GetComponent (typeof(IPowerUp)) as IPowerUp;
		//powerup.Armed = false;
		//audioTake = GetComponent<AudioSource> ();
		//cointText = GetComponentInChildren<Text>();
	}

	void Update ()
	{
		/*if (isFor != null && !lockisfor){
			isFor(powerup.ForAll);
			lockisfor = true;
		}*/
	}

	/*void OnMouseDown ()
	{
		if (count > 0)
			ActivatePowerUp ();
	}*/

	/*void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "Player") {
			ActivatePowerUp ();
		}
	}*/

	//POWER UPS
	public void ActivatePowerUp ()
	{
		if (count > 0){
			SoundManager.s.audios [2].Play ();
			Instantiate (sparks, transform.position, Quaternion.identity);
			BehaviorPlayer.iWantThePowerUp += TakePowerUp;
			//powerUpMessage.SetTrigger("PowerUp");
		//Rigidbody r =  GetComponent<Rigidbody>();
		//r.useGravity = false;
		//r.isKinematic = true;
		//transform.position = new Vector3(0,100,0);
			GameControl.g.SendPowerUpMessage (0);
			PowerUpsControl.p.EnableRayCast (powerup.ForAll, 8);

			count--;
			cointText.text = "x" + count;
			GameControl.userInfo.powerups[GameControl.userInfo.team.ipower].count = count;
		}
	}

	public void TakePowerUp (BehaviorPlayer playerPowerUp)
	{
		playerPowerUp.SetPowerUp (powerup, false);
		BehaviorPlayer.iWantThePowerUp -= TakePowerUp;
		//Destroy (gameObject);
	}

	public void SetCount(int count){
		this.count = count;
		cointText.text = "x"+this.count;
	}
}
