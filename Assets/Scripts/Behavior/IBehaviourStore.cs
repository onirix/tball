﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class IBehaviourStore: MonoBehaviour {
	public Button back;
	public IStoreItem atributteItem;
	public bool locked = false;
	public abstract void AddListener();
}
