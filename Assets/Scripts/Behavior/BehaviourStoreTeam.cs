﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviourStoreTeam : BehaviourStore
{

	void Start ()
	{
		item = StoreManager.s.teamsPanels.GetComponentInChildren<Button> ();
	}

	void OnMouseDown ()
	{
		if (!StoreManager.s.mainPanel.activeInHierarchy) {
			AddListener ();
			Display ();
			StoreManager.s.SetTeamsPanel ();
		}
	}

	public override void AddListener ()
	{
		back.onClick.RemoveAllListeners ();
		back.onClick.AddListener (() => StoreManager.s.HideTeamsPanels ());
	}
}
