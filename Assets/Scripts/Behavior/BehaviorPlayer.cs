﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviorPlayer : MonoBehaviour
{
	public delegate void IWantThePowerUp (BehaviorPlayer me);

	public static event IWantThePowerUp iWantThePowerUp;

	public delegate void SetPowerUpBar ();

	public static event SetPowerUpBar setPowerUpBar;

	public delegate void SetPowerUpBallEffect (IPowerUp powerup, int mode);

	public static event SetPowerUpBallEffect powerupBallEffect;

	public IPowerUp powerup;

	public ITeam team;

	public GameObject sparks;
	public Transform markPrefab;
	//public string teamHumanPlayer;

	private AudioSource audioRebound;
	private Transform localMark;
	private FieldPosition fieldposition;
	Animator reboundAnimator;
	Renderer meshrender;
	float timer;
	float delay;
	float intermitance = 0.5f;
	bool isforme = false;

	float bouncy;
	float aerea;
	int ignoreRayCastLayer;

	void Start ()
	{
		audioRebound = GetComponent<AudioSource> ();
		reboundAnimator = GetComponent<Animator> ();
		fieldposition = GetComponent<FieldPosition> ();
		meshrender = GetComponentInChildren<Renderer> ();
	}

	void Update ()
	{
		if (iWantThePowerUp != null) {
			//BehaviorPowerUp.isFor += IsForMe;
			if (isforme) {
				timer += Time.deltaTime;
				if (timer > intermitance) {
					if (meshrender.material.color != PowerUpsControl.p.defaultColor) {
						SetRendererColor (PowerUpsControl.p.defaultColor);
					} else {
						SetRendererColor (Color.green);
					}
					timer = 0;
				}
			}
		} else {
			if (isforme) {
				//meshrender.material.color = mycolor;
				isforme = false;
			}
		}

		if (powerup != null) {
			if (!powerup.Armed) {
				delay += Time.deltaTime;
				if (delay > 0.5f) {
					RaycastHit rayHit;
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

					if (Physics.Raycast (ray, out rayHit, 100, powerup.Mask)) {
						powerup.MouseButtonPosition (rayHit);

						if (Input.GetMouseButtonDown (0)) {
							powerup.MouseButtonDown (rayHit);
						}
						if (Input.GetMouseButtonUp (0)) {
							powerup.MouseButtonUp (rayHit);
						}
					}
				}
			} else {
				powerup.UpdatePowerUp ();
				if (!powerup.Armed) {
					Destroy (localMark.gameObject);
				}
			}
		}

	}

	void OnEnable ()
	{
		PowerUpsControl.enableRayCast += EnableRayCast;
	}

	void OnDisable ()
	{
		PowerUpsControl.enableRayCast -= EnableRayCast;
	}

	void OnMouseDown ()
	{
		if (iWantThePowerUp != null) {
			Instantiate (sparks, transform.position, Quaternion.identity);
			iWantThePowerUp (this);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Player") {
			reboundAnimator.SetTrigger ("Rebound");
			audioRebound.Play ();
			other.attachedRigidbody.velocity += team.GetArea ();
			GameControl.g.AddRebounds(tag);
			if (powerup != null) {
				if (powerup.Armed) {
					//if (powerup.ForAll == 3 || GameControl.g.isMyTurn ()) {
						powerup.Activate (other.GetComponent<Rigidbody> ());

						if (powerupBallEffect != null)
							powerupBallEffect (powerup, 1);
						//meshrender.material.color = mycolor;
						if (!powerup.Armed) {
							Destroy (localMark.gameObject);
							powerup = null;
						}
					/*} else {
						Debug.Log (name + "powerup is not forall: " + powerup.ForAll + ", or not is my turn: " + GameControl.g.isMyTurn ());
					}*/
				}
			}
		}
	}

	void EnableRayCast (int isfor, int raycast)
	{
		//if ((isfor & (fieldposition.team ^ GameControl.iam + 1)) != 0) {
		if ((isfor & (fieldposition.team + 1)) != 0) {
			gameObject.layer = raycast;
			isforme = true;
			if (raycast == 2) {
				SetRendererColor (PowerUpsControl.p.defaultColor);
			} else {
				SetRendererColor (Color.green);
			}
		}
	}

	/*void IsForMe (int forall)
	{
		Debug.Log (forall + " ISForMe: " + name);
		if ((forall & fieldposition.team + 1) != 0) {
			isforme = true;
			SetRendererColor (Color.red);
		}
		BehaviorPowerUp.isFor -= IsForMe;
	}*/

	public void SetTeam (ITeam team)
	{
		this.team = team;
		GetComponent<Collider> ().material.bounciness = team.bouncy;
	}

	public void SetPowerUp (IPowerUp powerup, bool computer)
	{
		//SetRendererColor (Color.red);
		this.powerup = powerup;
		if (powerupBallEffect != null)
			powerupBallEffect (this.powerup, 0);
		this.powerup.StartPowerUp (transform, computer);
		localMark = Instantiate (markPrefab, new Vector3 (transform.position.x, 0.5f, transform.position.z), Quaternion.identity) as Transform;
		localMark.Rotate (270, 0, 0);
		timer = 0;
		SoundManager.s.audios [1].Play ();
		if (!GameControl.g.computer) {
			//TowerControl.t.SetPowerUp();
		}
	}

	void SetRendererColor (Color color)
	{
		if (meshrender == null) {
			meshrender = GetComponentInChildren<Renderer> ();
		}
		meshrender.material.color = color;
	}
	/*public void SetPowerUp(){
		powerUp.ActivatePowerUp();
	}*/

}
