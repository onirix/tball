﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviorToggle : MonoBehaviour
{
	public delegate void IamOn (GameObject toggle);

	public static event IamOn iamOn;

	public delegate void SetFormation(int[] formation);

	public static event SetFormation setFormation;

	public int[] formation;

	private Toggle mytoggle;
	private bool selected = false;

	void Start ()
	{
		mytoggle = GetComponent<Toggle> ();
	}

	void Update ()
	{
		mytoggle.isOn = selected;
	}

	void OnEnable ()
	{
		BehaviorToggle.iamOn += IsMe;
		GameControl.isReady += IsReady;
	}

	void OnDisable ()
	{
		BehaviorToggle.iamOn -= IsMe;
		GameControl.isReady -= IsReady;
	}

	void IsMe (GameObject toggle)
	{
		if (gameObject != toggle) {
			selected = false;
		}
	}

	public void SetToggle ()
	{
		iamOn (gameObject);
		selected = true;
		//ArrangeFormation ();
		setFormation(formation);
	}

	/*void ArrangeFormation ()
	{
		float[] linePos = { -11.0f, -2.5f, 7.5f };
		int iteam = GameControl.iam + 1;
		int j = 0;
		int k = 1;
		float yards = 20.0f;
		float separation = yards / (formation [0] + 1);
		if (iteam == 2) {
			linePos [0] *= -1;
			linePos [1] *= -1;
			linePos [2] *= -1;
		}
		for (int i = 2; i < 12; i++) {
			Transform player = GameObject.Find ("player_" + iteam + i).GetComponent<Transform> ();
			FieldPosition playerFieldPosition = player.GetComponent<FieldPosition> ();
			Debug.Log ("player: " + player.name);
			player.position = new Vector3 (linePos [j], player.position.y, 9 - separation * (i - k));
			playerFieldPosition._x = player.position.x;
			playerFieldPosition._z = player.position.z;
			if ((i - k) % formation [j] == 0) {
				k = i;
				j++;
				if (j < 3) {
					separation = yards / (formation [j] + 1);
					Debug.Log ("i: " + i + "j: " + j + "k: " + k);
				}
			}
		}
	}*/

	void IsReady ()
	{
		Debug.Log("REady Detected!");
		gameObject.SetActive(false);
	}
}
