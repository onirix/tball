﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviorExpBar : MonoBehaviour {

	Slider expBar;
	float newExp;
	float timer;
	float exp;
	int speed = 2;

	void Start () {
		
	}

	void Update () {
		timer += Time.deltaTime;
		if (timer < newExp){
			expBar.value += timer*speed;
		}
	}

	void OnEnable(){
		GameControl.gameFinished += ExpProcess;
	}

	void OnDisable(){
		GameControl.gameFinished -= ExpProcess;
	}

	void ExpProcess(float exp, float gain){
		expBar.value = exp;
		newExp = gain;
	}
}
