﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BehaviourStore : IBehaviourStore
{
	public Button item;

	public override void AddListener ()
	{
	}

	public void Display ()
	{
		atributteItem.SetAttributes (item, locked);
		atributteItem.gameObject.SetActive (true);
		StoreManager.s.SetPortrait (item);
	}
}
