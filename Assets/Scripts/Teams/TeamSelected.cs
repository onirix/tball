﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TeamSelected : MonoBehaviour
{
	public delegate void OnTeamSelected(Sprite teamSprite, string teamName, string powerup, ITeam team);

	public event OnTeamSelected onTeamSelected;

	public Sprite teamPortrait;
	public string powerup;
	ITeam team;

	void Start(){
		team = gameObject.GetComponent (typeof(ITeam)) as ITeam;
	}
		
	void OnMouseDown ()
	{
		if (onTeamSelected!=null)
			onTeamSelected(teamPortrait, name, powerup, team);
	}
}
