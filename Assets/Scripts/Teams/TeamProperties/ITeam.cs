﻿using UnityEngine;
using System.Collections;

public interface ITeam {

	float bouncy {
		get;
	}
	float aerea {
		get;
	}
	string powerup {
		get;
	}

	Vector3 GetArea();
}
