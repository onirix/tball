﻿using UnityEngine;
using System.Collections;

public class TeamThief : MonoBehaviour, ITeam {

	private float _bouncy = 0.6f;

	private float _aerea = 0.8f;

	private string _powerup = "Invisible";

	public float bouncy {
		get {
			return _bouncy;
		}
	}

	public float aerea {
		get {
			return _aerea;
		}
	}

	public string powerup {
		get {
			return _powerup;
		}
	}

	public Vector3 GetArea(){
		return new Vector3(0,UnityEngine.Random.Range(0,_aerea),0);
	}
}
