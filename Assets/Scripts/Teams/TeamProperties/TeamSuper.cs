﻿using UnityEngine;
using System.Collections;

public class TeamSuper : MonoBehaviour, ITeam {

	private float _bouncy = 0.8f;

	private float _aerea = 0.3f;

	private string _powerup = "Direction";

	public float bouncy {
		get {
			return _bouncy;
		}
	}

	public float aerea {
		get {
			return _aerea;
		}
	}

	public string powerup {
		get {
			return _powerup;
		}
	}

	public Vector3 GetArea(){
		return new Vector3(0,UnityEngine.Random.Range(0,_aerea),0);
	}
}
