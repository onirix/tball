﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class FieldPosition : NetworkBehaviour
{
	public delegate void SelectionMode (string player, int num);

	public static event SelectionMode OnSelection;

	public delegate void OnPositionPlayer (Vector3 position);

	public static event OnPositionPlayer onPosition;

	/*GameObject selectHalo;
	Light selectHaloLight;*/
	public float _x;
	public float _z;

	[SyncVar]
	public int cteam = -1;
	[SyncVar]
	public bool selected = false;

	NetUser user;
	public int team;
	int selectableMask;
	int range = 100;
	int threshold = 2;
	public bool lockposition = false;


	// Use this for initialization
	void Start ()
	{
		/*selectHalo = GameObject.FindGameObjectWithTag("Selection");
		selectHaloLight = selectHalo.GetComponent<Light>();*/
		selectableMask = LayerMask.GetMask ("Floor");
		_x = transform.position.x;
		_z = transform.position.z;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (selected) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit rayHit;
			if (Physics.Raycast (ray, out rayHit, range, selectableMask)) {
				if (Mathf.Abs (rayHit.point.x - _x) <= threshold && Mathf.Abs (rayHit.point.z - _z) <= threshold) {
					//transform.position = new Vector3 (rayHit.point.x, transform.position.y, rayHit.point.z);
					//onPosition (new Vector3 (rayHit.point.x, transform.position.y, rayHit.point.z));
					//CmdSetPosition (new Vector3 (rayHit.point.x, transform.position.y, rayHit.point.z));
					if (user != null)
						user.SetPlayerPosition (new Vector3 (rayHit.point.x, transform.position.y, rayHit.point.z));
				}
			}
		}
	}

	void OnMouseDown ()
	{
		if (GameControl.g.fases == 0) {
			if (!lockposition) {
				if (tag == "Team" + (GameControl.iam + 1)) {
					//cteam = GameControl.iam;
					//OnSelection (name);
					if (user != null) {
						user.SetOnSelectionPlayer (name);
					}
				}
			}
		}
	}

	void OnEnable ()
	{
		GameControl.SetTeam += CurrentTeam;
		FieldPosition.OnSelection += SelectionPlayer;
		NetUser.setLocalPlayer += GetLocalPlayer;
	}

	void OnDisable ()
	{
		GameControl.SetTeam -= CurrentTeam;
		FieldPosition.OnSelection -= SelectionPlayer;
		NetUser.setLocalPlayer -= GetLocalPlayer;
	}

	public void PositionPlayer (Vector3 position)
	{
		if (selected)
			transform.position = position;
	}

	public void SelectionPlayer (string player, int num)
	{
		if (cteam == num) {
			if (player != name) {
				/*if (onPosition != null)
					onPosition -= PositionPlayer;*/
				selected = false;
			} else {
				selected = !selected;
				/*if (selected) {
					onPosition += PositionPlayer;
				} else {
					onPosition -= PositionPlayer;
				}*/
			}
		}
	}

	void CurrentTeam (int cTeam)
	{
		cteam = cTeam;
	}

	public void IsReady (NetUser user)
	{
		lockposition = true;
		//user.setSelection -= SelectionPlayer;
		user.iamReady -= IsReady;
	}

	void GetLocalPlayer (NetUser user)
	{
		this.user = user;
	}

	[Command]
	public void CmdSetPosition (Vector3 position)
	{
		transform.position = position;
	}

	[Command]
	public void CmdSetSelection (string name)
	{
		OnSelection (name, cteam);
	}
}
