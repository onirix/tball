﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetUser : NetworkBehaviour
{
	public delegate void SetPosition (Vector3 position);

	public event SetPosition setPosition;

	public delegate void SetSelection (string name);

	public event SetSelection setSelection;

	public delegate void IamReady (NetUser user);

	public event IamReady iamReady;

	public delegate void SetLocalPlayer (NetUser user);

	public static event SetLocalPlayer setLocalPlayer;

	[SyncVar]
	public string nick;
	[SyncVar]
	public string team;
	[SyncVar]
	public string powerup;
	[SyncVar]
	public int wins;
	[SyncVar]
	public int loses;
	[SyncVar]
	public int level;
	[SyncVar]
	public int ready = 0;
	[SyncVar]
	public int num;
	[SyncVar]
	public string messageServer;

	FieldPosition fieldPosition;
	float timerdelay;

	void Start ()
	{
		if (isLocalPlayer) {
			CmdSetUserInfo (GameControl.userInfo.srnck,
				GameControl.userInfo.team.name,
				GameControl.userInfo.powerups [GameControl.userInfo.team.ipower].name,
				GameControl.userInfo.wins,
				GameControl.userInfo.loses,
				GameControl.userInfo.level,
				GameControl.iam);
			if (setLocalPlayer != null)
				setLocalPlayer (this);
		}
	}

	void Update ()
	{
	}

	void OnEnable ()
	{
		GameControl.replenishTeamUser += ReplenishTeam;
		GameControl.setReadyUser += SetReady;
		GameControl.setTurnUser += SetTurn;
		GameControl.setPause += SetPause;
		FingerManager.hitBall += HitBall;
		//FieldPosition.OnSelection += SetOnSelectionPlayer;
		//FieldPosition.onPosition += SetPlayerPosition;
		BehaviorPlayer.powerupBallEffect += PowerUpBallEffect;
		BehaviorToggle.setFormation += ArrangeFormation;
	}

	void OnDisable ()
	{
		GameControl.replenishTeamUser -= ReplenishTeam;
		GameControl.setReadyUser -= SetReady;
		GameControl.setTurnUser -= SetTurn;
		GameControl.setPause -= SetPause;
		//FingerManager.hitBall -= HitBall;
		//FieldPosition.OnSelection -= SetOnSelectionPlayer;
		FieldPosition.onPosition -= SetPlayerPosition;
		BehaviorPlayer.powerupBallEffect -= PowerUpBallEffect;
		BehaviorToggle.setFormation -= ArrangeFormation;
	}

	[ClientRpc]
	void RpcRival (string nick, string team, int level, int wins, int loses)
	{
	}

	[Command]
	void CmdSetUserInfo (string nick, string team, string powerup, int wins, int loses, int level, int num)
	{
		this.nick = nick;
		this.team = team;
		this.powerup = powerup;
		this.wins = wins;
		this.loses = loses;
		this.level = level;
		this.num = num;
	}

	[Command]
	void CmdSetReady (int ready)
	{
		this.ready = ready;
	}

	[Command]
	void CmdSetTurn ()
	{
		GameNetworkControl.gn.SetTurn ();
	}

	[Command]
	void CmdSetPause (bool pause, int iam)
	{
		GameNetworkControl.gn.SetPause (pause, iam);
	}

	[Command]
	void CmdHitBall (Vector3 force)
	{
		GameNetworkControl.gn.ballhit = 2;
		GameNetworkControl.gn.rball.AddForce (force, ForceMode.Impulse);
	}

	[Command]
	void CmdPowerUpBallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position)
	{
		GameNetworkControl.gn.rball.velocity = velocity;
		GameNetworkControl.gn.rball.angularVelocity = angularVelocity;
		GameNetworkControl.gn.rball.position = position;
		GameNetworkControl.gn.rball.AddForce (force, ForceMode.Impulse);
	}

	[Command]
	void CmdRival (string nick, string team, int level, int wins, int loses)
	{
		RpcRival (nick, team, level, wins, loses);
	}

	[Command]
	void CmdReplenishTeam (string team, int iteam)
	{
		messageServer = "replenish command in server: " + nick;
		GameNetworkControl.gn.ReplenishTeam (team, iteam, null, this);
	}

	[Command]
	void CmdPlayerPosition (Vector3 position)
	{
		/*if (setPosition != null)
			setPosition (position, num);*/
		if (fieldPosition != null)
			fieldPosition.CmdSetPosition (position);
	}

	[Command]
	void CmdOnSelectionPlayer (string name)
	{
		fieldPosition = GameObject.Find (name).GetComponent<FieldPosition> ();
		if (fieldPosition != null)
			fieldPosition.CmdSetSelection (name);
		/*if (setSelection != null) {
			Debug.Log("setSelection ...");
			setSelection (name, num);
		}*/
	}

	[Command]
	void CmdArrangeFormation (int[]  formation, int iam)
	{
		GameNetworkControl.gn.ArrangeFormation (formation, iam);
	}

	[Command]
	void CmdSetVisitor (string visitor, string visitorteam)
	{
		GameNetworkControl.gn.CmdSetVisitor (visitor, visitorteam);
	}

	[Command]
	void CmdSetHome (string home, string homeTeam)
	{
		GameNetworkControl.gn.CmdSetHome (home, homeTeam);
	}

	void SetRival (string nick, string team, int level, int wins, int loses)
	{
		if (isLocalPlayer && GameControl.userInfo.srnck != nick) {
			GameControl.rival = nick;
			GameControl.rivalTeam = team;
			GameNetworkControl.gn.ReplenishTeam (GameControl.rivalTeam, 2 - GameControl.iam, null, this);
		}
	}

	void SetReady (int ready)
	{
		if (!isLocalPlayer) {
			return;
		}

		if (iamReady != null)
			iamReady (this);
		
		CmdSetReady (ready);
	}

	void SetTurn ()
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdSetTurn ();
	}

	void SetPause (bool pause)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdSetPause (pause, GameControl.iam);
	}

	public void HitBall (Vector3 force)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdHitBall (force);
	}

	public void PowerUpBallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdPowerUpBallEffect (force, velocity, angularVelocity, position);
	}

	void ReplenishTeam (string team, int iteam)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdReplenishTeam (team, iteam);
	}

	public void SetPlayerPosition (Vector3 position)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdPlayerPosition (position);
	}

	public void SetOnSelectionPlayer (string name)
	{
		if (!isLocalPlayer) {
			return;
		}
		CmdOnSelectionPlayer (name);
	}

	void PowerUpBallEffect (IPowerUp powerup, int mode)
	{
		powerup.PowerUpBallEffect (this, mode);
	}

	void ArrangeFormation (int[] formation)
	{
		CmdArrangeFormation (formation, GameControl.iam);
	}

}
