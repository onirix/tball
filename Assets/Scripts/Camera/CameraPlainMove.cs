﻿using UnityEngine;
using System.Collections;

public class CameraPlainMove : MonoBehaviour
{
	public float leftlimit = -12.0f;
	public float rightlimit = 15.0f;
	public float toplimit;
	public float bottomlimit;
	Vector2 deltaPos;
	Vector3 newCampos;
	float smooth = 1.0f;
	int mask;

	void Start ()
	{
		mask = LayerMask.GetMask ("Floor");
	}

	void Update ()
	{
		/*if (Input.touchCount > 0) {
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				deltaPos = Input.GetTouch (0).position;

			}
			if (Input.GetTouch (0).phase == TouchPhase.Ended) {
				deltaPos = deltaPos - Input.GetTouch (0).position;
				if (deltaPos.x > 2) {

				} else if (deltaPos.x < -2) {

				}
			}

		} else {
			
			if (Input.GetMouseButtonUp (0)) {
				newCampos = newCampos - Input.mousePosition;
				if (newCampos.x > 2) {
					
				} else if (newCampos.x < -2) {
					
				}
			}
		}*/
		if (Input.touchCount > 0) {
			if (Input.GetTouch(0).phase == TouchPhase.Moved){
				transform.position = Vector3.Lerp (transform.position, transform.position - new Vector3 (Input.GetTouch(0).deltaPosition.x, 0.0f, Input.GetTouch(0).deltaPosition.y), Time.deltaTime * smooth);
				if (transform.position.x > rightlimit) { 
					transform.position = new Vector3 (rightlimit, transform.position.y, transform.position.z);
				}
				if (transform.position.x < leftlimit) { 
					transform.position = new Vector3 (leftlimit, transform.position.y, transform.position.z);
				}
				if (transform.position.z > toplimit) { 
					transform.position = new Vector3 (transform.position.x, transform.position.y, toplimit);
				}
				if (transform.position.z < bottomlimit) { 
					transform.position = new Vector3 (transform.position.x, transform.position.y, bottomlimit);
				}
			}
		}
		/*if (Input.GetMouseButton (0)) {
			Debug.Log ("Detect mouse button");

			RaycastHit rayHit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out rayHit, 100, mask)) {
				transform.position = Vector3.Lerp (transform.position, new Vector3 (rayHit.point.x, transform.position.y, rayHit.point.z), Time.deltaTime * smooth);
				if (transform.position.x > rightlimit) { 
					transform.position = new Vector3 (rightlimit, transform.position.y, transform.position.z);
				}
				if (transform.position.x < leftlimit) { 
					transform.position = new Vector3 (leftlimit, transform.position.y, transform.position.z);
				}
				if (transform.position.z > toplimit) { 
					transform.position = new Vector3 (transform.position.x, transform.position.y, toplimit);
				}
				if (transform.position.z < bottomlimit) { 
					transform.position = new Vector3 (transform.position.x, transform.position.y, bottomlimit);
				}
			}
		}*/
	}
}
