﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

	/*public Transform target;
	public float smoothing = 5f;

	Vector3 offset;

	void Start(){
		offset = transform.position - target.position;
	}

	void FixedUpdate(){
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    	}*/
	public static CameraFollow c;
	//public Transform rig;
	public Transform player;
	// Reference to the player's transform.
	private Vector3 relCameraPos;
	// The relative position of the camera from the player.
	private float relCameraPosMag;
	// The distance of the camera from the player.
	public float smooth = 1.5f;
	private Vector3 newPos;
	// The position the camera is trying to reach.
	//Camera camera;
	float maxAngleRotation = 61.0f;
	float angleCameraRotation = 0.0f;
//	float cameraDistance = 22.5f;
	public int onRotate = 0;

	void Awake ()
	{
		//player = GameObject.FindGameObjectWithTag("Player").transform;
		//camera = GetComponent<Camera> ();
		c = this;
		CalPositions ();
	}

	void FixedUpdate ()
	{
		Vector3 standardPos = player.position + relCameraPos;

		//Vector3 abovePos = player.position + Vector3.up * relCameraPosMag;

		/*Vector3[] checkPoints = new Vector3[5];

		checkPoints [0] = standardPos;*/

		//checkPoints [1] = Vector3.Lerp (standardPos, abovePos, 0.25f);
		//checkPoints [2] = Vector3.Lerp (standardPos, abovePos, 0.5f);
		//checkPoints [3] = Vector3.Lerp (standardPos, abovePos, 0.75f);
		//checkPoints [4] = abovePos;

		/*int i;
		for (i = 0; i < checkPoints.Length; i++) {
			if (ViewingPosCheck (checkPoints [i]))
				break;
		}*/

		//if (CheckEchosOutOfView ()) {
		//	SmooothRotateAround ();
		//}else{

		//transform.position = Vector3.Lerp (transform.position, newPos, smooth * Time.deltaTime);

		transform.position = Vector3.Lerp (transform.position, standardPos, smooth * Time.deltaTime);
		SmoothLookAt ();
		//}
		/**if (onRotate != 0 || i == checkPoints.Length) {
			if (onRotate==0)
				onRotate = 1;**/
		
		/*if (onRotate!=0){	
			SmooothRotateAround ();
		}*/
	}

	void CalPositions ()
	{
		relCameraPos = transform.position - player.position;

		//relCameraPos = rig.position - player.position;
		relCameraPosMag = relCameraPos.magnitude - 0.5f;
	}

	bool ViewingPosCheck (Vector3 checkPos)
	{
		RaycastHit hit;

		if (Physics.Raycast (checkPos, player.position - checkPos, out hit, relCameraPosMag))
		if (hit.transform != player)
			return false;

		newPos = checkPos;
		return true;
	}

	void SmoothLookAt ()
	{
		Vector3 relPlayerPosition = player.position - transform.position;
		Quaternion lookAtRotation = Quaternion.LookRotation (relPlayerPosition, Vector3.up);
		transform.rotation = Quaternion.Lerp (transform.rotation, lookAtRotation, smooth * Time.deltaTime);
	}

	void SmooothRotateAround ()
	{
		CalPositions ();
		if (relCameraPos.magnitude > 22.5f) {
			ZoomInOut (1);
			return;
		} else if (relCameraPos.magnitude < 19f) {
			ZoomInOut (-1);
			return;
		} else if (relCameraPos.y < 12) {
			UpDowmCamera (1);
			return;
		} else if (relCameraPos.y > 14) {
			UpDowmCamera (-1);
			return;
		}
		float angle = Mathf.LerpAngle (0, maxAngleRotation - angleCameraRotation, Time.deltaTime);
		angleCameraRotation += angle;
		transform.RotateAround (player.position, Vector3.up, angle * onRotate);

		if (angleCameraRotation >= 60) {
			angleCameraRotation = 0;
			onRotate = 0;
		}

	}

	void UpDowmCamera (int updowm)
	{
		float step = 10 * Time.deltaTime * updowm;
		//Vector3 direction = player.position - transform.position;
		transform.position = Vector3.MoveTowards (transform.position, transform.position + Vector3.up * relCameraPosMag, step);
		//transform.position += inout*step*transform.forward;
	}

	void ZoomInOut (int inout)
	{
		float step = 10 * Time.deltaTime * inout;
		//transform.position = Vector3.MoveTowards(transform.position, target, step);
		transform.position = Vector3.MoveTowards (transform.position, new Vector3 (player.position.x, transform.position.y, player.position.z), step);
	}

	/*bool CheckEchosOutOfView ()
	{
		for (int i = 0; i < TimerManager.playerEchos.Count; i++) {
			if (TimerManager.playerEchos [i] != null) {
				Vector3 viewPos = camera.WorldToViewportPoint (TimerManager.playerEchos [i].echo.transform.position);
				if (viewPos.z < 0 || viewPos.x > 1 || viewPos.x < -1 || viewPos.y > 1 || viewPos.y < -1) {
					return true;
				}
			}
		}
		return false;
	}*/

	/*public void PerspectiveType ()
	{
		camera.orthographic = !camera.orthographic;
	}*/

	public void RotateCamera ()
	{
		onRotate = 1;
	}

	public void SetTarget (Transform target)
	{
		player = target;
	}

}
