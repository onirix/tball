﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BackDoor : MonoBehaviour
{
	public delegate void ExitAdvise ();

	public static event ExitAdvise exitAdvise;

	public int scene;

	void OnMouseDown ()
	{
		GameControl.rival = "";
		if (exitAdvise != null) {
			exitAdvise ();
			StartCoroutine (ExitDelay ());
		} else {
			SceneManager.LoadScene (scene);
		}
	}

	IEnumerator ExitDelay ()
	{
		yield return new WaitForSeconds (5);
		SceneManager.LoadScene (scene);
	}
}
