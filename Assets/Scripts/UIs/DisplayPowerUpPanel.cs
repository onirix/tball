﻿using UnityEngine;
using System.Collections;

public class DisplayPowerUpPanel : MonoBehaviour {

	public GameObject powerupPanel;
	bool display;

	public void DisplayPanel(){
		display = !display;
		powerupPanel.SetActive(display);
	}
}
