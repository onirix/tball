﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Renderer))]
[RequireComponent (typeof (Animator))]
public class OnSelect : MonoBehaviour {

	Color color;
	public Renderer rend;
	Animator anim;

	void Start () {
		rend = GetComponent<Renderer>();
		anim = GetComponent<Animator>();
		color = rend.material.color;
	}
	void OnMouseEnter(){
		rend.material.color = Color.green;
		anim.SetBool("OnSelect", true);
	}

	void OnMouseExit(){
		anim.SetBool("OnSelect", false);
		rend.material.color = color;
	}
}
