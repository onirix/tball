﻿using UnityEngine;
using System.Collections;

public class PlayerSelected : MonoBehaviour
{
	public delegate void SelectMe (string nick, string tmi);

	public static event SelectMe OnSelect;

	public void Select ()
	{
		GameControl.rival = gameObject.name;
		OnSelect (GameControl.rival, GameControl.userInfo.team.name);
	}
}
