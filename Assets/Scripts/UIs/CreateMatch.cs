﻿using UnityEngine;
using System.Collections;

public class CreateMatch : MonoBehaviour
{

	public delegate void CreateRoom ();

	public static event CreateRoom createRoom;

	public GameObject createPortrait;

	bool matchCreated = false;

	void OnMouseDown ()
	{
		if (!UIManager.ui.uilocked){
			UIManager.ui.uilocked = true;
			createPortrait.SetActive(true);
		}
	}

	void OnEnable ()
	{
		PlayersControl.matchCreatedSuccess += MatchCreated;
	}

	void OnDisable ()
	{
		PlayersControl.matchCreatedSuccess -= MatchCreated;
	}

	void MatchCreated ()
	{
		matchCreated = true;
		Destroy(GetComponent<OnSelect>());
	}

	public void ConfirmCreateRoom(){
		UIManager.ui.uilocked = false;
		createPortrait.SetActive(false);
		if (!matchCreated) {
			Debug.Log("Creating room...");
			createRoom ();
		}
	}

	public void HideCreatePotrait(){
		UIManager.ui.uilocked = false;
		createPortrait.SetActive(false);
	}
}
