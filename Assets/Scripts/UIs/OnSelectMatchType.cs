﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OnSelectMatchType : OnSelectPlayer
{

	public delegate void SetMatchType (int type);

	public static event SetMatchType setMatchType;

	public GameObject matchPortrait;
	public Image imagePortrait;
	public Text textPortrait;

	public Renderer board;

	public Sprite[] matchIcon;

	string[] types = { "Internet", "LocalNetwork" };

	int match = 0;

	void OnMouseDown ()
	{
		if (!UIManager.ui.uilocked) {
			UIManager.ui.uilocked = true;
			matchPortrait.SetActive (true);
		}
	}

	public void HideMatchPortrait ()
	{
		UIManager.ui.uilocked = false;
		matchPortrait.SetActive (false);
	}

	public void ConfirmMatchType ()
	{
		UIManager.ui.uilocked = false;
		matchPortrait.SetActive (false);
		if (setMatchType != null) {
			match = 1 - match;
			board.material.mainTexture = matchIcon [match].texture;
			imagePortrait.sprite = matchIcon [match];
			textPortrait.text = "Do you want search\n " + types [match] + " match?";
			setMatchType (match);
		}
	}
}
