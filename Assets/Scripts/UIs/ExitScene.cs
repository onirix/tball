﻿using UnityEngine;
using System.Collections;

public class ExitScene : MonoBehaviour
{

	public GameObject exitPanel;

	void OnMouseDown ()
	{
		if (!UIManager.ui.uilocked) {
			UIManager.ui.uilocked = true;
			exitPanel.SetActive (true);
		}
	}

	public void CancelExit ()
	{
		UIManager.ui.uilocked = false;
		exitPanel.SetActive (false);
	}
}
