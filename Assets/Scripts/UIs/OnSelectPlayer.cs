﻿using UnityEngine;
using System.Collections;

public class OnSelectPlayer : IWasSelectedClass
{
	public Vector3 scaleAnim;

	Vector3 defaultScale;
	bool onSelect = false;
	float timer;
	float speed = 2.0f;

	void Start ()
	{
		defaultScale = transform.localScale;
	}

	void Update ()
	{
		if (onSelect) {
			if (transform.localScale.x < scaleAnim.x || transform.localScale.y < scaleAnim.y || transform.localScale.z < scaleAnim.z) {
				timer += Time.deltaTime;
				transform.localScale = new Vector3 (Mathf.Min (transform.localScale.x * (1 + Time.deltaTime * speed), scaleAnim.x),
					Mathf.Min (transform.localScale.y * (1 + Time.deltaTime * speed), scaleAnim.y),
					Mathf.Min (transform.localScale.z * (1 + Time.deltaTime * speed), scaleAnim.z));
			}
		} else {
			if (transform.localScale.x > defaultScale.x || transform.localScale.y > defaultScale.y || transform.localScale.z > defaultScale.z) {
				timer += Time.deltaTime;
				transform.localScale = new Vector3 (Mathf.Max (transform.localScale.x * (1 - Time.deltaTime * speed), defaultScale.x),
					Mathf.Max (transform.localScale.y * (1 - Time.deltaTime * speed), defaultScale.y),
					Mathf.Max (transform.localScale.z * (1 - Time.deltaTime * speed), defaultScale.z));
			}
		}
	}

	void OnMouseEnter ()
	{
		onSelect = true;
	}

	void OnMouseExit ()
	{
		onSelect = false;
	}

	void OnMouseDown ()
	{
		selected = true;
		iWasSelected (gameObject);
	}
}
