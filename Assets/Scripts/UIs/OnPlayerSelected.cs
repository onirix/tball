﻿using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using UnityEngine;
using System.Collections;

public class OnPlayerSelected : MonoBehaviour
{
	
	public delegate void MatchSelectMe (NetworkID networkID);

	public static event MatchSelectMe OnMatchSelect;

	public delegate void LocalSelectMe (string networkAddress);

	public static event LocalSelectMe OnLocalSelect;

	public Button acceptButton;

	public string team;

	public NetworkID networkID;
	public string networkAddress;

	void OnMouseDown ()
	{
		acceptButton.onClick.RemoveAllListeners ();
		acceptButton.onClick.AddListener (
			() => JoinPlayer ()
		);
	}

	public void JoinPlayer ()
	{
		GameControl.rival = gameObject.name;
		GameControl.rivalTeam = team;
		if (networkAddress == "")
			OnMatchSelect (networkID);
		else
			OnLocalSelect (networkAddress);
	}
}
