﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public static SoundManager s;
	public AudioSource[] audios;
	// Use this for initialization
	void Awake () {
		s = this;
		audios = GetComponentsInChildren<AudioSource>();
	}
}
