﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;

public class GameNetworkControl : NetworkBehaviour
{
	public static GameNetworkControl gn;

	public delegate void SetReady ();

	public event SetReady setReady;

	public delegate void PlayerDisconnected ();

	public static event PlayerDisconnected playerDisconnected;

	public delegate void SetVisitor (string visitor, string visitorTeam);

	public static event SetVisitor setVisitor;

	public delegate void SetUsersConfiguration ();

	public static event SetUsersConfiguration setUsersConfiguration;

	//[SyncVar]
	public string home;
	//[SyncVar]
	public string visitor;
	//[SyncVar]
	public string homeTeam;
	//[SyncVar]
	public string visitorTeam;

	[SyncVar]
	public bool isgol = false;
	[SyncVar]
	public float timer;
	[SyncVar]
	public int nplayers = 2;
	[SyncVar]
	public int iplayer;
	[SyncVar]
	public bool allReady = false;
	[SyncVar]
	public int ballhit = 0;

	[SyncVar]
	float gameDuration = 600;
	[SyncVar]
	int turns = 0;
	//[SyncVar]
	int period = 1;

	int pause;

	bool clientReady;

	public Rigidbody rball;
	public Transform tball;
	public int[] players = new int[2];


	float goldelay = 0.0f;
	//float timergame = 0;


	string TIMESTR = "Time: ";
	Vector3 startPosition = new Vector3 (0, 0.5f, 0);

	Vector3 savedVelocity;
	Vector3 savedAngular;

	void Awake ()
	{
		gn = this;
		if (isLocalPlayer) {
			clientReady = false;
		}
	}

	void Update ()
	{
		if (isServer) {
			/*timergame += Time.deltaTime;
			if (timergame > 5) {
				Debug.Log ("iam in the server: " + GameControl.userInfo.srnck);
				Debug.Log ("players: " + TNetworkManager.n.net.numPlayers + ", allReady: " + allReady);
				timergame = 0;
			}*/
			if (allReady) {
				if (period < 2 && pause == 0) {
					timer += Time.deltaTime;

					if (timer > gameDuration) {
						RpcFinishGame ();
						timer = 0;
						period = 2;
					}
				}
				if (GameNetworkControl.gn.ballhit == 2) { 
					if (rball.velocity.magnitude < 2f && rball.angularVelocity.magnitude < 2f) {
						SetTurn ();
					}
				}
				if (isgol) {
					goldelay += Time.deltaTime;
					if (goldelay > 3.0f) {
						isgol = false;
						goldelay = 0.0f;
						tball.position = startPosition;
					}
				}
				if (!GameControl.g.computer) {
					if (NetworkServer.connections.Count == 1) {
						RpcPlayerDisconnected ();
					}
				}
			} else {
				foreach (NetworkConnection con in NetworkServer.connections) {
					if (con.playerControllers == null)
						continue;
					foreach (PlayerController player in con.playerControllers) {
						NetUser netUser = player.gameObject.GetComponent<NetUser> ();
						if (netUser.ready == 0) {
							return;
						} else {
							if (netUser.num == 0) {
								home = netUser.nick;
								homeTeam = netUser.team;
							} else {
								visitor = netUser.nick;
								visitorTeam = netUser.team;
							}
						}
					}
				}
				allReady = true;
				RpcStartGame (home, visitor, homeTeam, visitorTeam);
			}
		}

	}

	[Command]
	public void CmdSetHome (string home, string homeTeam)
	{
		this.home = home;
		this.homeTeam = homeTeam;
	}

	[Command]
	public void CmdSetVisitor (string visitor, string visitorTeam)
	{
		this.visitor = visitor;
		this.visitorTeam = visitorTeam;
	}

	/*[Command]
	public void CmdSetReady (int iam)
	{
		//iplayer = 1 - iam; 
		CheckReady(iam);
		Debug.Log ("Ready: " + ready);

	}*/

	[Command]
	public void CmdSetTurn ()
	{
		SetTurn ();
	}

	[Command]
	public void CmdSetGol (int i)
	{
		SoundManager.s.audios [3].Play ();
		rball.velocity = Vector3.zero;
		rball.angularVelocity = Vector3.zero;
		players [i]++;
		iplayer = 1 - i;
		isgol = true;
		RpcSetGol (i);
	}

	/*[ClientRpc]
	void RpcInitTeams ()
	{

		//StartCoroutine(WaitingForUser());
		
		if (GameControl.iam == 1) {
			Debug.Log("Sending visitor");
			CmdSetVisitor (GameControl.userInfo.srnck, GameControl.userInfo.team.name);
		} else {
			Debug.Log("Sending home");
			CmdSetHome (GameControl.userInfo.srnck, GameControl.userInfo.team.name);
		}
	}*/

	[ClientRpc]
	public void RpcStartGame (string home, string visitor, string homeTeam, string visitorTeam)
	{
		iplayer = 0;
		GameControl.g.StartGame (home, visitor, homeTeam, visitorTeam);
	}

	[ClientRpc]
	void RpcFinishGame ()
	{
		GameControl.g.FinishGame ();
	}

	[ClientRpc]
	void RpcPlayerDisconnected ()
	{
		playerDisconnected ();
	}

	[ClientRpc]
	void RpcStartPlayerTeam (string playerName, string teamName)
	{
		GameObject player = GameObject.Find (playerName);
		GameObject playerTeam = GameObject.Find (teamName);
		playerTeam.name = playerName + "team";
		playerTeam.transform.rotation = player.transform.rotation;
		playerTeam.transform.parent = player.transform;
		//NetworkTransformChild playerChild = player.AddComponent<NetworkTransformChild> ();
		//playerChild.target = playerTeam.transform;
		playerTeam.transform.localScale = new Vector3 (1.5f, 2.0f, 1.5f);
		ITeam team = playerTeam.GetComponent (typeof(ITeam)) as ITeam;
		player.GetComponent<BehaviorPlayer> ().SetTeam (team);

		try {
			Destroy (playerTeam.GetComponent<OnPlayerSelected> ());
		} catch (NullReferenceException ex) {
		}
	}

	[ClientRpc]
	void RpcSetGol (int i)
	{
		GameControl.g.RefreshScore (i);
	}

	public void SetTurn ()
	{
		turns++;
		iplayer = turns % nplayers;
		ballhit = 0;
	}

	public void SetPause (bool pause, int iam)
	{
		if (pause) {
			savedVelocity = rball.velocity;
			savedAngular = rball.angularVelocity;
			rball.Sleep ();
			if (iam == 0)
				this.pause |= 1;
			else
				this.pause |= 2;
		} else {
			rball.WakeUp ();
			rball.velocity = savedVelocity;
			rball.angularVelocity = savedAngular;
			if (iam == 0)
				this.pause &= 2;
			else
				this.pause &= 1;
		}
	}

	public bool iWin (int iam)
	{
		if ((iam == 0 && players [0] > players [1]) || (iam == 1 && players [1] > players [0])) {
			return true;
		}
		return false;
	}

	public void ReplenishTeam (string teamName, int iteam, PlayerPosition[] positions, NetUser netuser)
	{

		GameObject[] members = Resources.LoadAll<GameObject> ("Teams/" + teamName);
		GameObject[] players = GameObject.FindGameObjectsWithTag ("Team" + iteam);
		for (int i = 0; i < players.Length; i++) {
			players [i].layer = LayerMask.NameToLayer ("Shootable");
			int selected = 0;
			if (members.Length >= 2) {
				if (players [i].name == "player_" + iteam + "1") {
					selected = members.Length - 1;
				} else {
					if (members.Length >= 3) {
						selected = (int)Mathf.Round (UnityEngine.Random.Range (0.0f, members.Length - 2));
					}
				}
			}
			if (GameControl.g.computer) {
				players [i].transform.position = new Vector3 (UnityEngine.Random.Range (players [i].transform.position.x - 2, players [i].transform.position.x + 2), players [i].transform.position.y, UnityEngine.Random.Range (players [i].transform.position.z - 2, players [i].transform.position.z + 2));
			} else {
				if (positions != null && i < positions.Length) {
					players [i].transform.position = new Vector3 (positions [i].x, positions [i].y, positions [i].z);
					bool inver = false;
					int f = Convert.ToInt32 (players [i].name.Replace ("player_" + iteam, ""));
					if (iteam == 2) {
						if (f < 9) {
							if (players [i].transform.position.x < 0) {
								inver = true;
							}
						} else {
							if (players [i].transform.position.x > 0) {
								inver = true;
							}
						}
					} else {
						if (f < 9) {
							if (players [i].transform.position.x > 0) {
								inver = true;
							}
						} else {
							if (players [i].transform.position.x < 0) {
								inver = true;
							}
						}
					}
					if (inver) {
						players [i].transform.position = new Vector3 (-players [i].transform.position.x, players [i].transform.position.y, players [i].transform.position.z);
					}
				} 
			}


			GameObject instance = Instantiate (members [selected], new Vector3 (players [i].transform.position.x, 0.0f, players [i].transform.position.z), players [i].transform.rotation) as GameObject;

			if (GameControl.g.computer && iteam == 2 && GameControl.g.iaComputer.powerup == null) {
				ITeam team = instance.GetComponent (typeof(ITeam)) as ITeam;
				GameControl.g.iaComputer.LoadPowerUp (GameControl.g.LoadPowerUpResource (team.powerup));
			}
			if (isServer) {
				NetworkServer.Spawn (instance);
				FieldPosition playerField = players [i].GetComponent<FieldPosition> ();
				playerField.cteam = netuser.num;
				//netuser.setSelection +=	playerField.SelectionPlayer;
				//netuser.setPosition += playerField.PositionPlayer;
				if (positions == null) {
					if (netuser.num == 0) {
						instance.transform.Rotate (0, 90, 0);
					} else {
						instance.transform.Rotate (0, -90, 0);
					}
				}
			}
		}
	}

	public void ArrangeFormation (int[]  formation, int iam)
	{
		float[] linePos = { -11.0f, -2.5f, 7.5f };
		int iteam = iam + 1;
		int j = 0;
		int k = 1;
		float yards = 20.0f;
		float separation = yards / (formation [0] + 1);
		if (iteam == 2) {
			linePos [0] *= -1;
			linePos [1] *= -1;
			linePos [2] *= -1;
		}
		for (int i = 2; i < 12; i++) {
			Transform player = GameObject.Find ("player_" + iteam + i).GetComponent<Transform> ();
			FieldPosition playerFieldPosition = player.GetComponent<FieldPosition> ();
			player.position = new Vector3 (linePos [j], player.position.y, 9 - separation * (i - k));
			playerFieldPosition._x = player.position.x;
			playerFieldPosition._z = player.position.z;
			if ((i - k) % formation [j] == 0) {
				k = i;
				j++;
				if (j < 3) {
					separation = yards / (formation [j] + 1);
				}
			}
		}
	}
}
