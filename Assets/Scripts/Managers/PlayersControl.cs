﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.NetworkSystem;
using System;
using System.Collections;
using System.Collections.Generic;

public class PlayersControl : MonoBehaviour
{
	public delegate void MatchCreated ();

	public static event MatchCreated matchCreatedSuccess;

	public TowerControl towerControl;
	public GameObject playerPrefab;
	//public GameObject playersContainer;
	public GameObject playerNick;
	public GameObject disconnecting;

	public Sprite[] teamsSprites;

	public GameObject portraitPanel;
	public Image imagePortrait;
	public Text textPortrait;

	public GameObject atributtesPanel;
	public Slider attackBar;
	public Slider defenseBar;
	public Slider aereaBar;
	public Text powerUpText;
	public Image waitMessage;

	public Button acceptButton;

	public Sprite[] portraits;

	//public Text messageLog;
	//public Renderer referenceCam;

	List<MatchDesc> matchList = new List<MatchDesc> ();
	List<string> localMatchList = new List<string> ();
	bool matchCreated;
	bool gameStart;

	//NetworkMatch networkMatch;
	NetworkID networkID;
	NodeID nodeID;
	int currentPage = 0;
	int nextPage = 0;
	int matchType = 0;

	float timer;

	void Awake ()
	{
		timer = 0;
		gameStart = false;
	}

	void Start ()
	{

		TNetworkManager.n.dis.Initialize ();
		TNetworkManager.n.dis.StartAsClient ();
		TNetworkManager.n.net.StartMatchMaker ();

	}

	// Update is called once per frame
	void Update ()
	{

		if (TNetworkManager.n.isHost) { 
			if (TNetworkManager.n.net.numPlayers >= 2 && !gameStart) {
				Debug.Log("Changing loading scene...");
				gameStart = true;
				TNetworkManager.n.dis.StopBroadcast ();
				TNetworkManager.n.net.ServerChangeScene (GameProperties.stadiums [GameControl.userInfo.istadium]);
			}
		} else {
			timer += Time.deltaTime;

			if (timer > 5) {
				if (matchType == 0) {
					if (TNetworkManager.n.dis.broadcastsReceived != null) {
						foreach (KeyValuePair<string, NetworkBroadcastResult> s in TNetworkManager.n.dis.broadcastsReceived) {
							if (s.Value.serverAddress != null) {
								string networkAddress = s.Value.serverAddress.Replace ("::ffff:", "");
								string[] data = System.Text.Encoding.Unicode.GetString (s.Value.broadcastData).Split (':');
								if (!localMatchList.Contains (data [1]) && localMatchList.Count < 7) {
									InstantiatePlayer (data [1], data [2], new NetworkID (), networkAddress, matchList.Count);
									localMatchList.Add (data [1]);
								}
							}
						}
						TNetworkManager.n.dis.broadcastsReceived.Clear ();
					}
				} else {
					matchList.Clear ();
					TNetworkManager.n.net.matchMaker.ListMatches (0, 6, "", OnMatchList);
				}
				timer = 0;
			}
		}
	}

	void OnEnable ()
	{
		OnPlayerSelected.OnMatchSelect += JoinMatch;
		OnPlayerSelected.OnLocalSelect += JoinLocalMatch;
		CreateMatch.createRoom += CreateRoom;
		BackDoor.exitAdvise += DestroyMatch;
		OnSelectMatchType.setMatchType += SetMatchTyoe;
	}

	void OnDisable ()
	{
		OnPlayerSelected.OnMatchSelect -= JoinMatch;
		OnPlayerSelected.OnLocalSelect -= JoinLocalMatch;
		CreateMatch.createRoom -= CreateRoom;
		BackDoor.exitAdvise -= DestroyMatch;
		OnSelectMatchType.setMatchType -= SetMatchTyoe;
	}

	public void Back ()
	{
		GameControl.rival = "";
		if (NetworkServer.active) {
			TNetworkManager.n.net.StopHost ();
		} else if (NetworkClient.active) {
			TNetworkManager.n.net.StopClient ();
		}
		TNetworkManager.n.dis.StopBroadcast ();
		SceneManager.LoadScene (0);
	}

	IEnumerator RefreshMatchList ()
	{
		TNetworkManager.n.net.matchMaker.ListMatches (0, 6, "", OnMatchList);
		do {
			yield return new WaitForSeconds (10);
		} while(true);
	}

	public void CreateRoom ()
	{
		DestoryPlayers ();
		if (matchType == 0) {
			GameControl.iam = 0;
			GameControl.gmi = 1;
			TNetworkManager.n.net.networkPort = 7777;
			TNetworkManager.n.net.networkAddress = GameControl.userInfo.srnck;
			TNetworkManager.n.net.StartHost ();
			TNetworkManager.n.dis.StopBroadcast ();
			TNetworkManager.n.dis.broadcastData = TNetworkManager.n.net.networkPort + ":" + GameControl.userInfo.srnck + ":" + GameControl.userInfo.team.name;
			TNetworkManager.n.dis.StartAsServer ();

			InstantiatePlayer (GameControl.userInfo.srnck, GameControl.userInfo.team.name, networkID, "", -1);
			matchCreatedSuccess ();
		} else {
			CreateMatchRequest create = new CreateMatchRequest ();
			create.name = GameControl.userInfo.srnck + ":" + GameControl.userInfo.team.name;
			create.size = 2;
			create.advertise = true;
			create.password = "";
			create.matchAttributes = new Dictionary<string, long> ();
			create.matchAttributes.Add ("Level", GameControl.userInfo.level);
			create.matchAttributes.Add ("Stadium", GameControl.userInfo.istadium);

			TNetworkManager.n.net.matchMaker.CreateMatch (create, OnMatchCreate);
			Debug.Log("Creating match");
		}
		TNetworkManager.n.isHost = true;
		waitMessage.enabled = true;
	}

	public void MatchBoardFind (int page)
	{
		ListMatchRequest request = new ListMatchRequest ();
		request.pageNum = page;
		request.pageSize = 6;
		TNetworkManager.n.net.matchMaker.ListMatches (request, OnMatchList);
	}

	public void JoinMatch (NetworkID networkID)
	{
		TNetworkManager.n.net.matchMaker.JoinMatch (networkID, "", OnMatchJoined);
	}

	public void JoinLocalMatch (string networkAddress)
	{

		GameControl.iam = 1;
		GameControl.gmi = 1;
		TNetworkManager.n.net.networkAddress = networkAddress;
		TNetworkManager.n.net.networkPort = 7777;
		TNetworkManager.n.net.StartClient ();
	}

	public void DestroyMatch ()
	{
		disconnecting.SetActive (true);
		if (networkID != null) {
			if (TNetworkManager.n.isHost) {
				TNetworkManager.n.net.matchMaker.DestroyMatch (networkID, OnExitMatch);
			} else {
				DropConnectionRequest dropReq = new DropConnectionRequest ();
				dropReq.networkId = (NetworkID)networkID;
				dropReq.nodeId = (NodeID)nodeID;
				TNetworkManager.n.net.matchMaker.DropConnection (dropReq, OnExitMatch);
			}
		} 
		if (NetworkServer.active)
			TNetworkManager.n.net.StopHost ();
		else if (NetworkClient.active)
			TNetworkManager.n.net.StopClient ();
		if (TNetworkManager.n.dis.running)
			TNetworkManager.n.dis.StopBroadcast ();
	}

	public void OnMatchList (ListMatchResponse response)
	{
		if (response.matches.Count == 0) {
			if (currentPage == 0) {
			} else {
				nextPage = currentPage;
				currentPage--;
			}

			return;
		} else if (response.matches.Count == 6) {
			nextPage++;
		}

		//Debug.Log("Match list: " + response.matches.Count);

		for (int i = 0; i < response.matches.Count; ++i) {
			string[] data = response.matches [i].name.Split (':');
			if (data[0] != GameControl.userInfo.srnck){
				InstantiatePlayer (data [0], data [1], response.matches [i].networkId, "", i);
			}
		}
	}

	void InstantiatePlayer (string name, string teamName, NetworkID networkID, string networkAddress, int i)
	{
		GameObject[] team = Resources.LoadAll<GameObject> ("Teams/" + teamName);
		if (team.Length > 0) {
			int selected = (int)Mathf.Round (UnityEngine.Random.Range (0.0f, team.Length - 1));
			GameObject instance = Instantiate (team [selected], new Vector3 (6.0f * (i % 3 - 1), 1.5f, 5.0f * Mathf.RoundToInt (1 - i / 3)), this.transform.rotation) as GameObject;
			instance.transform.localScale = new Vector3 (2.0f, 3.0f, 2.0f);
			instance.name = name;

			OnSelectPlayer onSelectPlayer = instance.GetComponent<OnSelectPlayer> ();
			if (i < 0) {
				Destroy (onSelectPlayer);
				Destroy (instance.GetComponent<OnPlayerSelected> ());
				instance.transform.position = new Vector3 (0.0f, 1.5f, 0.0f);
				instance.transform.Rotate (0, 0, 0);
			} else {
				TeamSelected teamSelected = instance.AddComponent<TeamSelected> ();
				teamSelected.onTeamSelected += ActivePortrait;
				foreach (Sprite portrait in portraits) {
					if (portrait.name.IndexOf (teamName) != -1) {
						teamSelected.teamPortrait = portrait;
					}
				}
				onSelectPlayer.scaleAnim = new Vector3 (2.2f, 3.2f, 2.2f);
				OnPlayerSelected onPlayerSelected = instance.GetComponent<OnPlayerSelected> ();
				if (networkID != null && networkID.ToString () != "0") {
					onPlayerSelected.networkID = networkID;
				} else {
					onPlayerSelected.networkAddress = networkAddress;
				}
				onPlayerSelected.acceptButton = acceptButton;
			}
			GameObject nick = (GameObject)Instantiate (playerNick, instance.transform.position, Quaternion.identity);
			nick.transform.parent = instance.transform;
			nick.name = name + "_Text";
			TextMesh nickText = nick.GetComponent<TextMesh> ();
			if (nickText != null) {
				nickText.text = name;
				if (i < 0) {
					nickText.color = Color.green;
				}
			}
		}
	}

	public void OnMatchCreate (CreateMatchResponse matchResponse)
	{
		if (matchResponse.success) {
			Debug.Log("Match created!");
			//TNetworkManager.n.net.onlineScene = GameProperties.stadiums [GameControl.userInfo.istadium];
			matchCreated = true;
			networkID = matchResponse.networkId;
			Utility.SetAccessTokenForNetwork (matchResponse.networkId, new NetworkAccessToken (matchResponse.accessTokenString));
			TNetworkManager.n.net.StartHost (new MatchInfo (matchResponse));

			InstantiatePlayer (GameControl.userInfo.srnck, GameControl.userInfo.team.name, networkID, TNetworkManager.n.net.networkAddress, -1);
			matchCreatedSuccess ();
		}
	}

	public void OnExitMatch (BasicResponse response)
	{
		SceneManager.LoadScene (0);
	}

	public void OnMatchJoined (JoinMatchResponse matchJoin)
	{
		if (matchJoin.success) {

			if (matchCreated) {
				return;
			}
			Utility.SetAccessTokenForNetwork (matchJoin.networkId, new NetworkAccessToken (matchJoin.accessTokenString));
			TNetworkManager.n.net.StartClient (new MatchInfo (matchJoin));
			GameControl.gmi = 1;
			GameControl.iam = 1;
		} 
	}

	public void OnConnected (NetworkMessage msg)
	{
		GameControl.gmi = 1;
	}

	public void OnLocalClientConnected (NetworkMessage msg)
	{
		TNetworkManager.n.dis.StopBroadcast ();
	}

	public void OnPlayerReadyMessage (NetworkMessage msg)
	{
		string message = msg.reader.ReadString ();
		if (message != "") {
			TNetworkManager.n.net.ServerChangeScene (GameProperties.stadiums [GameControl.userInfo.istadium]);
		}
	}

	void PlayersResponse (string response, long responseCode)
	{
		try {
			int rerror = Convert.ToInt32 (response);
		} catch (FormatException ex) {
			string[] players = response.Split (':');
			for (int i = 0; i < players.Length; i++) {
				string[] data = players [i].Split (',');
				GameObject[] team = Resources.LoadAll<GameObject> ("Teams/" + data [1]);
				if (team.Length > 0) {
					int selected = (int)Mathf.Round (UnityEngine.Random.Range (0.0f, team.Length - 1));
					GameObject instance = Instantiate (team [selected], new Vector3 (6.0f * (i % 3 - 1), 1.5f, 5.0f * Mathf.RoundToInt (1 - i / 3)), this.transform.rotation) as GameObject;
					instance.transform.localScale = new Vector3 (2.0f, 3.0f, 2.0f);
					instance.tag = "Team";
					instance.name = data [0];

					OnSelectPlayer onSelectPlayer = instance.GetComponent<OnSelectPlayer> ();
					onSelectPlayer.scaleAnim = new Vector3 (2.2f, 3.2f, 2.2f);

					GameObject nick = (GameObject)Instantiate (playerNick, instance.transform.position, Quaternion.identity);
					nick.name = data [0] + "_Text";
					TextMesh nickText = nick.GetComponent<TextMesh> ();
					if (nickText != null) {
						nickText.text = data [0];
					}
				}
			}
		}
	}

	void JoinResponse (string response, long responseCode)
	{
		try {
			string[] dataGame = response.Split (':');
			if (dataGame.Length == 3) {
				GameControl.gmi = Convert.ToInt32 (dataGame [0]);
				GameControl.userInfo.istadium = Convert.ToInt16 (dataGame [1]);
				GameControl.rivalTeam = dataGame [2];
				GameControl.iam = 1;
				SceneManager.LoadScene (GameControl.userInfo.istadium + 6);
			}
		} catch (FormatException ex) {
		}
	}

	void SetMatchTyoe (int matchType)
	{
		this.matchType = matchType;
		if (matchType == 1) {
			TNetworkManager.n.dis.StopBroadcast ();
			TNetworkManager.n.net.matchMaker.ListMatches (0, 6, "", OnMatchList);
		} else {
			TNetworkManager.n.dis.StartAsClient ();
		}
		DestoryPlayers ();
	}

	void DestoryPlayers ()
	{
		GameObject[] players = GameObject.FindGameObjectsWithTag ("Team");
		foreach (GameObject player in players) {
			player.GetComponent<TeamSelected> ().onTeamSelected -= ActivePortrait;
			Destroy (player);
		}
	}

	public void ActivePortrait (Sprite teamSprite, string teamName, string powerup, ITeam team)
	{
		if (!UIManager.ui.uilocked) {
			UIManager.ui.uilocked = true;
			imagePortrait.sprite = teamSprite;
			textPortrait.text = teamName;
			portraitPanel.SetActive (true);

		}
	}

	public void HidePortraits ()
	{
		UIManager.ui.uilocked = false;
		portraitPanel.SetActive (false);
		//atributtesPanel.SetActive (false);
	}
}
