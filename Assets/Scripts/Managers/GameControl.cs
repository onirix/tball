﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//using admob;

using GoogleMobileAds;
using GoogleMobileAds.Api;

//Spawn
public class GameControl : MonoBehaviour
{
	public delegate void SetTeamTurn (int team);

	public static event SetTeamTurn SetTeam;

	public delegate void IsReady ();

	public static event IsReady isReady;

	public delegate void GameFinished (float exp, float gain);

	public static event GameFinished gameFinished;

	public delegate void SetReadyUser (int ready);

	public static event SetReadyUser setReadyUser;

	public delegate void SetTurnUser ();

	public static event SetTurnUser setTurnUser;

	public delegate void ReplenishTeamUser (string team, int iteam);

	public static event ReplenishTeamUser replenishTeamUser;

	public delegate void SetPause (bool pause);

	public static event SetPause setPause;

	public static GameControl g;
	// statics deleted
	public static UserInfo userInfo;
	public static int gmi = 0;
	public static int iam = 0;
	public static string rival = "";
	public static string rivalTeam = "";

	//public NetUser me;
	//public GameObject powerUpObject;
	public Image powerupMessage;
	public Sprite[] powerupMessages;
	public int rebounds;
	public int threshold;
	public bool computer = false;
	//end statics

	//public static int istadium = 0;
	//public static int iteam = 0;
	public GameObject ballPrefab;
	public GameObject gncPrefab;
	public Rigidbody rball;
	public Transform tball;
	public AudioSource golMark;
	public Button ready;
	public Text[] playersScore;
	public Text formation;
	public Text chronometer;
	public Text gameTime;
	public Text responseServer;
	public Text coinsResult;
	public Text coinsTotal;
	public Image waitMessage;
	public Image powerUpBarColor;
	public Image expImage;
	public Image winlose;
	public Image n1;
	public Image n2;
	public Sprite computerMatchImg;
	public Sprite lose;
	public Sprite[] nums;
	public Slider powerupBar;
	public Slider expBar;
	public GameObject freeCamera;

	public BehaviorPowerUp[] slots;
	public TowerControl towerControl;
	public IComputer iaComputer;

	public float wtimer;
	public float stimer;
	public float ptimer;
	public int fases = 0;

	//private RewardBasedVideoAd rewardVideo;

	private InterstitialAd interstitial;

	//GameObject selectHalo;
	//int teamLayer;

	//	float powerUpsTime = 10;
	float cpunit;
	float leveup = 100.0f;


	//public int ballhit = 0;

	//color powerup bar unit

	string TIMESTR = "Time: ";
	string GAMETIMESTR = "Game Period: ";
	string FORMATIONSTR = "Player 2 formation.";
	string WAITINGRIVAL = "Waiting for rival or click";
	string PLAYERSTR = "Player";
	string TWOPOINTSTR = ": ";
	string teamHumanPlayer;

	Vector3 startPosition = new Vector3 (0, 0.5f, 0);

	void Awake ()
	{
		g = this;
	}

	void Start ()
	{

		teamHumanPlayer = "Team" + (iam + 1);
		waitMessage.enabled = false;
		//selectHalo = GameObject.FindGameObjectWithTag ("Selection");
		//rball.AddForce ((new Vector3(1,0,1)) * 50, ForceMode.Impulse);

		//SetTeam (iam);
		//Debug.Log ("gmi: " + gmi + " team: " + userInfo.team);
		//SetReady (iam << 1);
		powerupMessage = GameObject.FindGameObjectWithTag ("PowerUp").GetComponent<Image> ();
		powerupMessages = Resources.LoadAll <Sprite> ("Sprites/PowerUpMessages");
		for (int i = 0; i < userInfo.powerups.Count; i++) {
			GameObject powerUpObject = Resources.Load ("PowerUps/" + userInfo.powerups [i].name) as GameObject;
			IPowerUp powerup = powerUpObject.GetComponent (typeof(IPowerUp)) as IPowerUp;
			Image slotImage = slots [i].gameObject.GetComponent<Image> ();
			slotImage.sprite = powerUpObject.GetComponent<Image> ().sprite;
			slotImage.color = Color.white;
			powerup.Armed = false;
			if (i == userInfo.team.ipower) {
				powerupBar.maxValue = threshold = powerup.Threshold - userInfo.level;
				powerupBar.value = 0;
				cpunit = 2.0f / threshold;
			}

			slots [i].index = i;
			slots [i].SetCount (userInfo.powerups [i].count);
			slots [i].powerup = powerup;
		}
		if (gmi != -1) {
			freeCamera.SetActive (false);
			if (iam == 1) {
				formation.text = FORMATIONSTR;
			}
		}

		StartCoroutine (WaitingForUser ());
		RequestInterstitial ();
		/*Admob.Instance ().initAdmob ("ca-app-pub-3032381466155348/5869880118", "ca-app-pub-3032381466155348/8964240917");
		Admob.Instance ().setTesting (true);
		Admob.Instance ().loadRewardedVideo ("ca-app-pub-3032381466155348/8964240917"); */
	}



	// Update is called once per frame
	void Update ()
	{
		if (GameNetworkControl.gn != null)
			chronometer.text = TIMESTR + ((int)(GameNetworkControl.gn.timer / 60)) + ":" + ((int)(GameNetworkControl.gn.timer % 60));


		waitMessage.enabled = (GameNetworkControl.gn.ballhit == 2);

		formation.text = "Turn: player" + (GameNetworkControl.gn.iplayer + 1);
		if (computer && GameNetworkControl.gn.ballhit == 0) {
			if (fases == 2 && iam != GameNetworkControl.gn.iplayer) {
				StartCoroutine (SetComputerTurn ());
			} 
		}

	}

	IEnumerator WaitingForUser ()
	{
		while (replenishTeamUser == null) {
			yield return new WaitForSeconds (2);
		}

		int iteam = (iam + 1);

		replenishTeamUser (userInfo.team.name, iteam);
		if (gmi == -1) {
			SetComputerMatch ();
		}
	}

	public void Spawn ()
	{
		NetworkServer.Spawn (tball.gameObject);
		NetworkServer.Spawn (gncPrefab);
	}


	void OnEnable ()
	{
		TowerControl.OnGameExit += ExitGameResponse;
		TowerControl.OnPositions += PositionsResponse;
		BehaviorPlayer.setPowerUpBar += setPowerupBar;
	}

	void OnDisable ()
	{
		TowerControl.OnGameExit -= ExitGameResponse;
		TowerControl.OnPositions -= PositionsResponse;
		BehaviorPlayer.setPowerUpBar -= setPowerupBar;
	}

	public void StartGame (string home, string visitor, string homeTeam, string visitorTeam)
	{
		fases = 2;
		formation.text = "Turn: player" + (GameNetworkControl.gn.iplayer + 1);
		freeCamera.SetActive (true);
		ready.gameObject.SetActive (false);
		//if (rivalTeam == "") {
		GameObject[] rivalplayers; 
		if (iam == 0) {
			rivalTeam = visitorTeam;
			rivalplayers = GameObject.FindGameObjectsWithTag ("Team2");
		} else {
			rivalTeam = homeTeam;
			rivalplayers = GameObject.FindGameObjectsWithTag ("Team1");
		}
		/**GameObject[] rivalmembers = Resources.LoadAll<GameObject> ("Teams/" + rivalTeam);
		if (rivalmembers.Length > 0) {
			ReplenishTeam (rivalmembers, rivalplayers, 2 - iam, null);
		}**/
		//}
	}

	public void SetReady ()
	{
		if (fases == 0) {
			
			formation.text = WAITINGRIVAL;
			GameObject[] players = GameObject.FindGameObjectsWithTag ("Team" + (iam + 1));
			userInfo.positions = new PlayerPosition[players.Length];
			for (int i = 0; i < players.Length; i++) {
				players [i].layer = LayerMask.NameToLayer ("Ignore Raycast");
				userInfo.positions [i] = new PlayerPosition (players [i].transform.position.x, players [i].transform.position.y, players [i].transform.position.z);
			}
			SaveGame ();

			if (setReadyUser != null)
				setReadyUser (iam + 1);
			fases = 1;

			ready.gameObject.SetActive (false);
		} else {//start computer match
			towerControl.GameExit (gmi, "FORFE");
			ready.gameObject.SetActive (false);
			if (!computer) {
				SetComputerMatch ();
			}
			freeCamera.SetActive (true);

			fases = 2;
			setTurn ();
		}
		if (isReady != null) {
			isReady ();
		}
			
	}

	public void PositionsResponse (string response, long responseCode)
	{
		string[] vectors = response.Split (';');
		GameObject[] rivals = GameObject.FindGameObjectsWithTag ("Team" + (2 - iam));
		if (vectors.Length == rivals.Length) {
			for (int i = 0; i < vectors.Length; i++) {
				Vector3 pos = towerControl.ResponseToVector (vectors [i]);
				if (pos.x != 1000)
					rivals [i].transform.position = pos;
			}
		}
	}

	public void ExitGame ()
	{
		if (TNetworkManager.n.net.isNetworkActive) {
			if (TNetworkManager.n.isHost) {
				TNetworkManager.n.net.StopHost ();
			} else {
				TNetworkManager.n.net.StopClient ();
			}
		}

		SceneManager.LoadScene (0);
	}

	public void ExitGameResponse (string response, long responseCode)
	{
		if (!computer)
			SceneManager.LoadScene (0);
	}

	public void FinishGame ()
	{
		fases = 4;
		bool win = false;
		float expGain = 0.0f;
		int aimode = 1;
		int coins = 0;
		winlose.enabled = true;
		expImage.enabled = true;
		expBar.gameObject.SetActive (true);
		n1.enabled = true;
		n2.enabled = true;
		coinsResult.enabled = true;
		coinsTotal.enabled = true;
		SetLevelSprite ();

		if (computer) {
			aimode = 2;
		}

		if (GameNetworkControl.gn.iWin (iam)) {
			userInfo.wins++;
			expGain = ((100 + GameNetworkControl.gn.players [iam] * 10 - GameNetworkControl.gn.players [1 - iam] * 5) / (userInfo.level + 1)) / aimode;
			if (expGain < 0)
				expGain = 0;
			if (computer) {
				userInfo.history++;
			}
		} else {
			winlose.GetComponent<Image> ().sprite = lose;
			userInfo.loses++;
			expGain = ((GameNetworkControl.gn.players [iam] * 10 - GameNetworkControl.gn.players [1 - iam] * 5) / (userInfo.level + 1)) / aimode;
		}
		if (gameFinished != null)
			gameFinished (userInfo.exp, expGain);
		
		userInfo.exp += expGain;
		coins = 10 * (GameNetworkControl.gn.players [iam] - GameNetworkControl.gn.players [1 - iam]);
		if (coins < 0)
			coins = 0;
		userInfo.coins += coins;
		coinsResult.text = "Coins: " + coins;
		coinsTotal.text = "total coins: " + userInfo.coins;

		if (interstitial.IsLoaded ()) {
			interstitial.Show ();
		} else {
			StartCoroutine (BarFillAnimation ());
		}

		/*if (rewardVideo != null && rewardVideo.IsLoaded ()) {
			rewardVideo.Show ();
		} else {
			StartCoroutine (BarFillAnimation ());
		}*/

		/*if (Admob.Instance ().isRewardedVideoReady ()) {
			//AdmobEvent.onAdFailedToLoad = "HandleOnAdFailedToLoad";
			Admob.Instance ().rewardedVideoEventHandler += HandleRewardBasedVideoRewarded;
			//AdmobEvent.onAdClosed = "HandleOnAdClosed";
			// Called when the ad click caused the user to leave the application.
			//AdmobEvent.onAdLeftApplication = "HandleOnAdLeavingApplication";

			//AdmobEvent.onRewarded += HandleRewardBasedVideoRewarded;		
			Admob.Instance ().showRewardedVideo ();
		} else {
			StartCoroutine (BarFillAnimation ());
		}*/

	}

	IEnumerator BarFillAnimation ()
	{

		SaveGame ();
		yield return new WaitForSeconds (2);
		float finish = Mathf.Min (100.0f, userInfo.exp) - 1;
		while (expBar.value < finish) {
			expBar.value += Mathf.Lerp (expBar.value, userInfo.exp, Time.deltaTime * 0.01f);
			yield return null;
		}
		expBar.value = userInfo.exp;

		if (setLevelUp ()) {
			finish = userInfo.exp - 1;
			while (expBar.value < finish) {
				expBar.value += Mathf.Lerp (expBar.value, userInfo.exp, Time.deltaTime * 0.01f);
				yield return null;
			}
			expBar.value = userInfo.exp;
		}
		Invoke ("ExitGame", 5);
	}

	public void setTurn ()
	{
	
		if (setTurnUser != null) {
			setTurnUser ();
			waitMessage.enabled = false;
			ptimer = 0;
		}
	}

	public void setGol (int i)
	{
		SoundManager.s.audios [3].Play ();
		GameNetworkControl.gn.CmdSetGol (i);
	}

	public void AddRebounds (string tag)
	{
		if (tag == teamHumanPlayer) {
			if (isPowerUpReady ()) {
				rebounds++;
				VerifyRebounds ();
			}
		} else if (computer) {
			iaComputer.rebounds++;
			if (iaComputer.rebounds >= iaComputer.threshold) {
				iaComputer.rebounds = 0;
				iaComputer.SetPowerUp ();
			}
		}
	}

	void VerifyRebounds ()
	{
		if (rebounds >= threshold) {
			rebounds = 0;
			AddPowerUp ();
		}
		setPowerupBar ();
	}

	public void RefreshScore (int i)
	{
		playersScore [i].text = PLAYERSTR + (i + 1) + TWOPOINTSTR + GameNetworkControl.gn.players [i];
		if (i == iam) {
			rebounds += 100;
			VerifyRebounds ();
		}
	}

	bool setLevelUp ()
	{
		if (userInfo.exp > leveup) {
			if (userInfo.level < 99) {
				userInfo.level++;
				userInfo.exp = (userInfo.exp - leveup) / userInfo.level;
				SetLevelSprite ();
				return true;
			}
		}
		return false;
	}

	void SetLevelSprite ()
	{
		n1.sprite = nums [(int)userInfo.level / 10]; 
		n2.sprite = nums [(int)userInfo.level % 10];
	}

	void setPowerupBar ()
	{
		powerupBar.value = rebounds;
		float v = cpunit * rebounds / 2;
		if (rebounds < threshold / 2) {
			powerUpBarColor.color = new Color (1.0f, v, 0.0f, 1.0f); 
		} else {
			powerUpBarColor.color = new Color ((1.0f - v), 1.0f, 0.0f, 1.0f); 
		}
	}

	public void FreeCamera ()
	{
		fases = (fases == 2) ? 3 : 2;
	}

	public bool isMyTurn ()
	{
		return GameNetworkControl.gn.iplayer == iam;
	}


	void SetComputerMatch ()
	{
		computer = true;
		iaComputer = new ComputerEasy ();
		string[] rivals = {
			"Alien",
			"Eye",
			"Army",
			"Bananaman",
			"Church",
			"Deer",
			"Bear",
			"Hazmat",
			"Lord",
			"Programmer",
			"Raccoon",
			"Robot",
			"Sumo",
			"Super",
			"Vampire",
			"Thief",
			"Zombie",
			"Worker",
			"Worship",
			"Zombie"
		};
		int selectedTeam;
		if (userInfo.history < rivals.Length) {
			selectedTeam = userInfo.history;
		} else {
			selectedTeam = Mathf.RoundToInt (UnityEngine.Random.Range (0.0f, rivals.Length - 1));
		}
		if (replenishTeamUser != null)
			replenishTeamUser (rivals [selectedTeam], 2);

		tball.gameObject.GetComponent<NetworkIdentity> ().enabled = false;
		tball.gameObject.GetComponent<NetworkTransform> ().enabled = false;

		iaComputer.target = GameObject.Find ("archs").GetComponent<Transform> ().GetChild (0).position;
	}

	IEnumerator SetComputerTurn ()
	{
		GameNetworkControl.gn.ballhit = 1;
		do {
			yield return new WaitForSeconds (3);
		} while(fases != 2 || GameNetworkControl.gn.isgol);
		iaComputer.HitBall (rball);
	}

	public GameObject InstantiateBall ()
	{
		GameObject ball = Instantiate (ballPrefab, new Vector3 (0.0f, 0.5f, 0.0f), Quaternion.identity) as GameObject;
		tball = ball.GetComponent<Transform> ();
		rball = ball.GetComponent<Rigidbody> ();
		return ball;
	}

	public void InstantiatePowerUp (int index, int count, Vector3 pos, GameObject powerUpObject)
	{
		GameObject powerupObject = GameObject.Instantiate (powerUpObject, pos, UnityEngine.Random.rotation) as GameObject;
	}

	public void AddPowerUp ()
	{
		userInfo.powerups [userInfo.team.ipower].count++;
		slots [userInfo.team.ipower].SetCount (userInfo.powerups [userInfo.team.ipower].count);
	}

	public void SendPowerUpMessage (int i)
	{
		if (i < 0) {
			powerupMessage.sprite = powerupMessages [0];
			powerupMessage.enabled = false;
			fases = 2;
			Camera.main.GetComponent<CameraFollow> ().enabled = true;
			Camera.main.GetComponent<CameraPlainMove> ().enabled = false;
			setPause (false);
		} else {
			powerupMessage.enabled = true;
			powerupMessage.sprite = powerupMessages [i];
			fases = 5;
			Camera.main.GetComponent<CameraFollow> ().enabled = false;
			Camera.main.GetComponent<CameraPlainMove> ().enabled = true;
			setPause (true);
		}

	}

	public bool isPowerUpReady ()
	{
		return !GameControl.g.powerupMessage.enabled;
	}

	public IPowerUp LoadPowerUpResource (string powerup)
	{
		GameObject powerUpObject = Resources.Load ("PowerUps/" + powerup) as GameObject;
		return powerUpObject.GetComponent (typeof(IPowerUp)) as IPowerUp;
	}

	public static void SaveGame ()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/tballGameInfo.dat");

		bf.Serialize (file, userInfo);
		file.Close ();
	}

	public static  void LoadGame ()
	{
		if (File.Exists (Application.persistentDataPath + "/tballGameInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/tballGameInfo.dat", FileMode.Open);
			userInfo = bf.Deserialize (file) as UserInfo;
			file.Close ();
		} else {
			userInfo = new UserInfo ();
		}
	}

	//ADESENSE
	private void RequestInterstitial ()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3032381466155348/1085464519";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd (adUnitId);

		interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;

		interstitial.OnAdClosed += HandleOnAdClosed;
		// Called when the ad click caused the user to leave the application.
		interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ()
		                    //		.AddTestDevice(AdRequest.TestDeviceSimulator)
		                    //		.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
		.AddKeyword ("game")
		                    //		.TagForChildDirectedTreatment(false)
		.Build ();
		// Load the interstitial with the request.
		interstitial.LoadAd (request);
	}

	/*private void RequestInterstitial ()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3032381466155348/1040897715";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		rewardVideo = RewardBasedVideoAd.Instance;

		rewardVideo.OnAdFailedToLoad += HandleOnAdFailedToLoad;

		rewardVideo.OnAdClosed += HandleOnAdClosed;
		// Called when the ad click caused the user to leave the application.
		rewardVideo.OnAdLeavingApplication += HandleOnAdLeavingApplication;

		rewardVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ()
		                    //		.AddTestDevice(AdRequest.TestDeviceSimulator)
		                    //		.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
		.AddKeyword ("game")
		                    //		.TagForChildDirectedTreatment(false)
		.Build ();
		// Load the interstitial with the request.
		rewardVideo.LoadAd (request, adUnitId);
	}*/

	/*public void HandleOnAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
	//public void HandleOnAdFailedToLoad (string eventName, string msg)
	{
		//print ("Interstitial Failed to load: " + args.Message);

	}

	public void HandleOnAdClosed (object sender, EventArgs args)
	//public void HandleOnAdClosed (string eventName, string msg)
	{
		//interstitial.Destroy ();
		StartCoroutine (BarFillAnimation ());
	}

	public void HandleOnAdLeavingApplication (object sender, EventArgs args)
	//public void HandleOnAdLeavingApplication (string eventName, string msg)
	{
		//interstitial.Destroy ();
		StartCoroutine (BarFillAnimation ());
	}

	public void HandleRewardBasedVideoRewarded (object sender, Reward args)
	//public void HandleRewardBasedVideoRewarded (string eventName, string msg)
	{
		userInfo.exp += (int) args.Amount;
		StartCoroutine (BarFillAnimation ());
	}*/
	public void HandleOnAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
	{
		//print ("Interstitial Failed to load: " + args.Message);

	}

	public void HandleOnAdClosed (object sender, EventArgs args)
	{
		interstitial.Destroy ();
		SceneManager.LoadScene (0);
	}

	public void HandleOnAdLeavingApplication (object sender, EventArgs args)
	{
		interstitial.Destroy ();
		SceneManager.LoadScene (0);
	}
}
