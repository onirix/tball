﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TeamsManager : MonoBehaviour
{
	public static TeamsManager t;

	public GameObject portraitPanel;
	public Image imagePortrait;
	public Text textPortrait;

	public GameObject atributtesPanel;
	public Slider attackBar;
	public Slider defenseBar;
	public Slider aereaBar;
	public Text powerUpText;

	GameObject[] teamsHiden;

	void Awake ()
	{
		t = this;
	}

	void Start ()
	{
		teamsHiden = GameObject.FindGameObjectsWithTag ("Team");
		int unlock = 7;
		if (GameControl.userInfo.level > unlock) {
			unlock = Mathf.Min (teamsHiden.Length, GameControl.userInfo.level);
		}
		for (int i = 0; i < unlock; i++) {
			teamsHiden [i].GetComponent<TeamSelected> ().onTeamSelected += ActivePortrait;
		}
		for (int i = unlock; i < teamsHiden.Length; i++) {
			Renderer teamRenderer = teamsHiden [i].GetComponentInChildren<Renderer> ();
			teamRenderer.material.color = Color.black;
			Destroy (teamsHiden [i].GetComponent<OnSelectPlayer> ());
			Destroy (teamsHiden [i].GetComponent<TeamSelected> ());
		}
	}

	public void ActivePortrait (Sprite teamSprite, string teamName, string powerup, ITeam team)
	{
		if (!UIManager.ui.uilocked) {
			imagePortrait.sprite = teamSprite;
			textPortrait.text = teamName;
			portraitPanel.SetActive (true);

			attackBar.value = team.bouncy;
			defenseBar.value = 1 - team.bouncy;
			aereaBar.value = team.aerea;
			powerUpText.text = powerup;
			atributtesPanel.SetActive (true);
		}
	}

	public void TeamSelectedConfirm ()
	{
		int ipower;
		for (ipower = 0; ipower < GameControl.userInfo.powerups.Count; ipower++) {
			if (GameControl.userInfo.powerups [ipower].name == powerUpText.text) {
				break;
			}
		}

		if (ipower == GameControl.userInfo.powerups.Count)
			GameControl.userInfo.powerups.Add (new PowerUpsStored (powerUpText.text, 0));

		GameControl.userInfo.team = new Team (textPortrait.text, ipower);
		GameControl.SaveGame ();
		SceneManager.LoadScene ("Start");
	}

	public void HidePortraits ()
	{
		UIManager.ui.uilocked = false;
		portraitPanel.SetActive (false);
		atributtesPanel.SetActive (false);
	}

	public void ExitScene ()
	{
		SceneManager.LoadScene ("Start");
	}
}
