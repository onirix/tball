﻿using UnityEngine;
using UnityEngine.Experimental.Networking;
using System.Collections;

public class TowerControl: MonoBehaviour
{
	public delegate void RegisterRes (string response, long responseCode);

	public static event RegisterRes OnRegister;

	public delegate void PositionsRes (string response, long responseCode);

	public static event PositionsRes OnPositions;

	public delegate void GetAppIdRes (string response, long responseCode);

	public static event GetAppIdRes OnGetAppId;

	public delegate void GamExitRes (string response, long responseCode);

	public static event GamExitRes OnGameExit;

	public delegate void PlayersRes (string response, long responseCode);

	public static event PlayersRes OnPlayers;

	public delegate void CreateGameRes (string response, long responseCode);

	public static event CreateGameRes OnCreateGame;

	public delegate void JoinGameRes (string response, long responseCode);

	public static event JoinGameRes OnJoinGame;

	public delegate void GetItRes (string response, long responseCode);

	public static event GetItRes OnGetIt;

	public delegate void ImWaitingRes (string response, long responseCode);

	public static event ImWaitingRes OnWaiting;

	public delegate void VerifiyPowerUpRes (string response, long responseCode);

	public static event VerifiyPowerUpRes OnPowerUps;


	//public int responseIndex = -1;
	public static TowerControl t;
	public static int waitingTime = 1;
	public static string TRES = "t:";

	string http = "http://";
	string host = "jodaxion.com/";
	//	string host = "localhost/";
	string folder = "tball/";
	string playerslist = "tbllrqspls.php";
	string waitinggame = "tbllrqswtn.php";
	string creategame = "tbllrqscrtg.php";
	string actiongame = "tbllrqsrdy.php";
	string actionplayer = "tbllrqsctn.php";
	string joingame = "tbllrqsjng.php";
	string getappid = "tbllgtidrgs.php";
	string getpositions = "tbllrqsgpps.php";
	string register = "tbllrqsrgs.php";
	string getballpos = "tbllrqsgps.php";
//	string setballpos = "tbllrqspsb.php";
	string setpowerup = "tbllrqspw.php";
	//string appidf = "appid";
	//string appidd = "";
	//string srmlf = "srml";
	//string srmld = "";
	string requestcontrol;
	string[,] rdata;

	void Awake(){
		t = this;
	}

	public IEnumerator Send (int responseIndex)
	{
		WWWForm form = new WWWForm ();
		form.AddField ("appid", GameControl.userInfo.appid);
		form.AddField ("srml", GameControl.userInfo.srml);
		if (rdata != null) {
			for (int i = 0; i < rdata.GetLength (0); i++) {
				form.AddField (rdata [i, 0], rdata [i, 1]);
			}
		}
		Debug.Log ("request: " + requestcontrol);
		using (UnityWebRequest www = UnityWebRequest.Post (requestcontrol, form)) {
			yield return www.Send ();

			if (www.isError) {
				Debug.Log (www.error);
				Debug.Log(www.responseCode);
				SetResponseServer(responseIndex,"", www.responseCode);
			} else {
				Debug.Log ("response: " + www.downloadHandler.text);
				Debug.Log("reposnseindex: "+responseIndex);
				SetResponseServer(responseIndex, www.downloadHandler.text, www.responseCode);
				//responseIndex = -1;
			}
		}
	}

	void SetResponseServer(int responseIndex, string response, long responseCode){
		switch (responseIndex) {
		case 0:
			OnGameExit (response, responseCode);
			break;
		case 1:
			OnCreateGame (response, responseCode);
			break;
		case 2:
			OnJoinGame (response, responseCode);
			break;
		case 3:
			OnGetIt (response, responseCode);
			break;
		case 4:
			OnWaiting (response, responseCode);
			break;
		case 5:
			OnRegister (response, responseCode);
			break;
		case 6:
			OnGetAppId (response, responseCode);
			break;
		case 7:
			OnPlayers (response, responseCode);
			break;
		case 8:
			Debug.Log("Responding positions");
			OnPositions (response, responseCode);
			break;
		case 9:
			OnPowerUps(response, responseCode);
			break;
		}
	}

	public void Register ()
	{
		//responseIndex = 5;

		rdata = new string[1, 2];
		rdata [0, 0] = "srnck";
		rdata [0, 1] = GameControl.userInfo.srnck;

		requestcontrol = http + host + folder + register;
		StartCoroutine (Send (5));
	}

	public void GetAppId ()
	{
		//responseIndex = 6;

		requestcontrol = http + host + folder + getappid;
		StartCoroutine (Send (6));
	}

	public void GetPositions(string rival){
		
		//responseIndex = 8;

		rdata = new string[1, 2];
		rdata [0, 0] = "rvl";
		rdata [0, 1] = rival;

		requestcontrol = http + host + folder + getpositions;
		StartCoroutine (Send (8));
	}

	public void GameStart ()
	{
		
	}

	public void SetFase (int gameid, string fase, PlayerPosition[] positions)
	{
		//responseIndex = 4;

		rdata = new string[2 + positions.Length, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		rdata [1, 0] = "ctn";
		rdata [1, 1] = fase;
		for (int i=0; i < positions.Length; i++){
			rdata [i + 2, 0] = "pos"+i;
			rdata [i + 2, 1] = positions[i].x.ToString("F6") + "," + positions[i].y.ToString("F6") + "," + positions[i].z.ToString("F6");
		}
		Debug.Log ("fase; " + fase + " gameid: " + gameid);

		requestcontrol = http + host + folder + actiongame;
		StartCoroutine (Send (4));
	}

	public void Players ()
	{
		//responseIndex = 7;

		requestcontrol = http + host + folder + playerslist;
		StartCoroutine (Send (7));
	}

	public void GameExit (int gameid, string type)
	{
		Debug.Log ("Exit game from server...");
		//responseIndex = 0;

		rdata = new string[2, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		rdata [1, 0] = "ctn";
		rdata [1, 1] = type;

		requestcontrol = http + host + folder + actiongame;
		StartCoroutine (Send (0));
	}

	public void CreateGame (string tmg, int std)
	{
		//responseIndex = 1;

		rdata = new string[2, 2];
		rdata [0, 0] = "tmg";
		rdata [0, 1] = tmg;
		rdata [1, 0] = "std";
		rdata [1, 1] = "" + std;

		requestcontrol = http + host + folder + creategame;
		StartCoroutine (Send (1));
	}

	public void JoinGame (string nick, string tmi)
	{
		//responseIndex = 2;

		rdata = new string[2, 2];
		rdata [0, 0] = "nck";
		rdata [0, 1] = nick;
		rdata [1, 0] = "tmi";
		rdata [1, 1] = tmi;

		requestcontrol = http + host + folder + joingame;
		StartCoroutine (Send (2));
	}

	public void GetIt (Vector3 dir, Vector3 pos,int gameid)
	{
		//responseIndex = 3;

		rdata = new string[3, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		rdata [1, 0] = "ctn";
		rdata [1, 1] = dir.x.ToString ("F6") + "," + dir.y.ToString ("F6") + "," + dir.z.ToString ("F6");
		rdata [1, 1] += ";" + pos.x.ToString ("F6") + "," + pos.y.ToString ("F6") + "," + pos.z.ToString ("F6");
		rdata [2, 0] = "ply";
		rdata [2, 1] = "" + GameControl.iam;
		//rdata [1, 1] += ";" + ang.x.ToString ("F6") + "," + ang.y.ToString ("F6") + "," + ang.z.ToString ("F6");

		requestcontrol = http + host + folder + actionplayer;
		StartCoroutine (Send (3));
	}

	public void SetPowerUp(string player, string powerup,int gameid){

		rdata = new string[3, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		rdata [1, 0] = "ctn";
		rdata [1, 1] = player;
		rdata [1, 1] += ";" + powerup;
		rdata [2, 0] = "ply";
		rdata [2, 1] = "" + GameControl.iam;
		//rdata [1, 1] += ";" + ang.x.ToString ("F6") + "," + ang.y.ToString ("F6") + "," + ang.z.ToString ("F6");

		requestcontrol = http + host + folder + actionplayer;
		StartCoroutine (Send (3));
	}

	public void IamWaiting (int gameid)
	{
		//responseIndex = 4;

		rdata = new string[1, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		/*rdata [1, 0] = "pos";
		rdata [1, 1] = pos.x.ToString ("F6") + "," + pos.y.ToString ("F6") + "," + pos.z.ToString ("F6");
		rdata [1, 1] += ";" + vel.x.ToString ("F6") + "," + vel.y.ToString ("F6") + "," + vel.z.ToString ("F6");
		rdata [1, 1] += ";" + ang.x.ToString ("F6") + "," + ang.y.ToString ("F6") + "," + ang.z.ToString ("F6");*/

		//Debug.Log("WTN: "+pos);

		requestcontrol = http + host + folder + waitinggame;
		StartCoroutine (Send (4));
	}

	public void VerifyPowerUps (int gameid)
	{
		//responseIndex = 4;

		rdata = new string[1, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;

		requestcontrol = http + host + folder + setpowerup;
		StartCoroutine (Send (9));
	}

	public void GetBallPos (int gameid)
	{
		//responseIndex = 4;

		rdata = new string[1, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;

		requestcontrol = http + host + folder + getballpos;
		StartCoroutine (Send (4));
	}

	/*public void SetBallPos (int iam, int gameid, Vector3 pos)
	{
		responseIndex = -1;

		rdata = new string[3, 2];
		rdata [0, 0] = "gmi";
		rdata [0, 1] = "" + gameid;
		rdata [1, 0] = "iam";
		rdata [1, 1] = "" + iam;
		rdata [2, 0] = "pos";
		rdata [2, 1] = pos.x.ToString ("F6") + "," + pos.y.ToString ("F6") + "," + pos.z.ToString ("F6");


		requestcontrol = http + host + folder + setballpos;
		StartCoroutine (Send ());
	}*/


	public Vector3 ResponseToVector (string strVector)
	{
		string[] coords = strVector.Split (',');
		if (coords.Length == 3) {
			return new Vector3 (float.Parse (coords [0]), float.Parse (coords [1]), float.Parse (coords [2]));
		}
		return new Vector3 (1000, 1000, 1000);
	}
}
