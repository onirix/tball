﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class RegisterControl : MonoBehaviour {

	public GameObject popup;
	public TowerControl towerControl;
	public Image backgroundPanel;
	public InputField mail;
	public InputField nick;
	///public Text mensaje;

	// Use this for initialization
	void Start () {
		int i = Mathf.RoundToInt(UnityEngine.Random.Range(1,3));
		Sprite background = Resources.Load<Sprite>("Sprites/Background0" + i);
		backgroundPanel.sprite = background;
		mail.text = GameControl.userInfo.srml;
		nick.text = GameControl.userInfo.srnck;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable ()
	{
		TowerControl.OnRegister += RegisterResponse;
	}

	void OnDisable ()
	{
		TowerControl.OnRegister -= RegisterResponse;
	}

	public void Register(){
		if (mail.text.Length == 0 || mail.text.IndexOf("@") == -1 || nick.text.Length == 0){
			return;
		}

		GameControl.userInfo.srml = mail.text;
		GameControl.userInfo.srnck = nick.text;

		towerControl.Register();
	}
	public void RegisterResponse(string response, long responseCode){
		//mensaje.text = response;
		if (response != "F"){
			/*PlayerPrefs.SetString ("srml", GameProperties.srml);
			PlayerPrefs.SetString ("srnck", GameProperties.srnck);
			PlayerPrefs.SetInt("registered", Convert.ToInt16(response));
			PlayerPrefs.Save ();*/
			GameControl.userInfo.registered = Convert.ToInt16(response);
			GameControl.SaveGame();
			ReturnToMainMenu();
		}else{
			GameControl.rival = "";
			Text message = popup.GetComponentInChildren<Text> ();
			message.text = "Sorry, register fail, please try later";
			Button button = popup.GetComponentInChildren<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(() => { popup.SetActive(false);});
			popup.SetActive (true);
		}
	}

	public void Back(){
		if (GameControl.userInfo.registered != 2){
			Text[] message = popup.GetComponentsInChildren<Text> ();
			if (GameControl.userInfo.registered == 1){
				message [0].text = "Remember confirm your email for play online matches!";
			}else{
				message [0].text = "Remember register a valid mail for play online matches!";
			}
			Button button = popup.GetComponentInChildren<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(() => {SceneManager.LoadScene(0);});
			popup.SetActive (true);
		}
	}

	public void ReturnToMainMenu(){
		SceneManager.LoadScene(0);
	}
}
