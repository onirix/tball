﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using System.Collections;

public class FingerManager : MonoBehaviour
{
	//public GameControl gameControl;
	//public TowerControl towerControl;

	public delegate void HitBall (Vector3 force);

	public static event HitBall hitBall;

	//public Rigidbody ball;

	int range = 100;
	int selectableMask;
	int shotableMask;
	int failCount;
	Vector3 startPoint;
	Vector3 endPoint;
	Vector3 failVector;
	Vector3 ballDirection;

	float timer;
	//	bool ballShot = false;

	// Use this for initialization
	void Start ()
	{
		failVector = new Vector3 (1000, 1000, 1000);
		shotableMask = LayerMask.GetMask ("Shootable");
		selectableMask = shotableMask | LayerMask.GetMask ("Selectable") | LayerMask.GetMask ("Floor");
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if (gameControl.fases == 2) {
		if (GameControl.g.fases == 2 && GameControl.g.isMyTurn ()) {
			//Debug.Log ("velocity: " + ball.velocity.magnitude + " angular velocity: " + ball.angularVelocity.magnitude);
			if (GameNetworkControl.gn.ballhit == 0) {
				
				/*if (Input.touchCount > 0) {
					if (Input.GetTouch (0).phase == TouchPhase.Began) {
						Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
						startPoint = Intersection (ray, range, selectableMask);

					}
					if (Input.GetTouch (0).phase == TouchPhase.Ended) {
						float speed = Input.GetTouch (0).deltaPosition.magnitude;
						Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
						endPoint = Intersection (ray, range, selectableMask);
						Debug.Log (startPoint + ", " + endPoint);

						if (startPoint != failVector && endPoint != failVector) {
							ballDirection = endPoint - startPoint;
							ballDirection = Mathf.Min (10, ballDirection.magnitude * speed) * ballDirection.normalized;
							Vector3 tstart = new Vector3 (startPoint.x, 0.5f, startPoint.z);
							Ray rayShot = new Ray (tstart, ballDirection.normalized);
							if (Intersection (rayShot, (int)ballDirection.magnitude, shotableMask) == failVector) {
								ballDirection = Vector3.zero;
								failCount++;
								if (failCount > 4) {
									failCount = 0;
								} else {
									return;
								}
							}
							//Debug.Log ("Get it: " + ballDirection);
							//if (GameControl.g.computer){
							if (hitBall != null) {
								hitBall (ballDirection);
								//ball.AddForce ((ballDirection) * ballDirection.magnitude, ForceMode.Impulse);

								////GameControl.g.ballhit = 2;
								Debug.Log ("i hit: " + GameNetworkControl.gn.ballhit + " " + Time.time);
							}

						}
					}
				}*/

				timer += Time.deltaTime;
				if (timer > 20) {
					if (hitBall != null) {
						timer = 0;
						hitBall (Vector3.zero);
					}
				}
				if (Input.GetMouseButtonDown (0)) {
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					startPoint = Intersection (ray, range, selectableMask);
				}
				if (Input.GetMouseButtonUp (0)) {
					float speed = 5;
					Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					endPoint = Intersection (ray, range, selectableMask);
					//Debug.Log (startPoint + ", " + endPoint);

					if (startPoint != failVector && endPoint != failVector) {
						ballDirection = endPoint - startPoint;
						ballDirection = Mathf.Min (40, ballDirection.magnitude * speed) * ballDirection.normalized;
						Vector3 tstart = new Vector3 (startPoint.x, 0.5f, startPoint.z);
						Ray rayShot = new Ray (tstart, ballDirection.normalized);
						if (Intersection (rayShot, (int)ballDirection.magnitude, shotableMask) == failVector) {
							ballDirection = Vector3.zero;
							failCount++;
							if (failCount > 4) {
								failCount = 0;
							} else {
								return;
							}
						}
						//Debug.Log ("Get it: " + ballDirection);
						//if (GameControl.g.computer){
						if (hitBall != null) {
							timer = 0;
							hitBall (ballDirection);
							//ball.AddForce ((ballDirection) * ballDirection.magnitude, ForceMode.Impulse);

							////GameControl.g.ballhit = 2;
//							Debug.Log ("i hit: " + GameNetworkControl.gn.ballhit + " " + Time.time);
						}

					}
				}
			}
		}
	}

	/*void OnEnable ()
	{
		TowerControl.OnGetIt += GetItResponse;
	}

	void OnDisable ()
	{
		TowerControl.OnGetIt -= GetItResponse;
	}

	public void GetItResponse (string response, long responseCode)
	{
		GameControl.g.responseServer.text = "G: " + response;
		string[] dataResponse = response.Split (';');
		if (dataResponse.Length == 3) {
			//response = response.Replace (TowerControl.TRES, "");
			GameControl.g.timer = Convert.ToInt64 (dataResponse [2]);
			Vector3 ballDirection = towerControl.ResponseToVector (dataResponse [0]);
			Vector3 ballPosition = towerControl.ResponseToVector (dataResponse [1]);
			if (ballDirection.x != 1000 && ballPosition.x != 1000) {
				
				ball.velocity = Vector3.zero;
				ball.angularVelocity = Vector3.zero;
				ball.position = ballPosition;
				//Debug.Log ("direction: " + ballDirection + " magnitude: " + ballDirection.magnitude + " posution: " + ballPosition);
				ball.AddForce ((ballDirection) * ballDirection.magnitude, ForceMode.Impulse);	
			}
			GameControl.g.setTurn ();
		}
	}*/

	private Vector3 Intersection (Ray ray, int r, int mask)
	{
		RaycastHit rayHit;
		if (Physics.Raycast (ray, out rayHit, r, mask)) {
			/*if (mask == shotableMask) {
				Debug.Log ("rayHit object: " + rayHit.point);
			}*/
			return rayHit.point;
		}
		return failVector;
	}

}
