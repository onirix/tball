﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class StoreManager : MonoBehaviour
{

	public static StoreManager s;

	public GameObject itemStoredPrefab;

	public GameObject mainPanel;
	public GameObject teamsPanels;
	public GameObject powerupsPanel;
	public GameObject portraitPanel;
	public GameObject teamAttributePanel;
	public GameObject powerupAttributePanel;

	List<GameObject> teamsStored;
	List<GameObject> powerupsStored;

	bool powerupsStarted;
	bool teamsStarted;

	Vector2 startPosition;
	int cols = 9;

	//public Button back;

	void Awake ()
	{
		s = this;
		startPosition = new Vector2 (124.5f, -24);
		teamsStored = new List<GameObject> ();
		powerupsStored = new List<GameObject> ();
	}


	public void StartPowerUpsItems ()
	{
		if (GameControl.userInfo.powerups != null) {
			GameObject[] items = GameObject.FindGameObjectsWithTag ("PowerUp");
			List<PowerUpsStored> powerups = GameControl.userInfo.powerups;
			int j = 0;
			for (int i = 0; i < powerups.Count; i++) {
				GameObject itemStored = GameObject.Instantiate (itemStoredPrefab);
				Rect itemRect = itemStored.GetComponent<Rect> ();
				j = (int)i / cols;
				itemRect.position = new Vector2 (i * 33, -47 * j);
				itemStored.GetComponentInChildren<Text> ().text = "x" + powerups [i].count;
				foreach (GameObject item in items) {
					if (item.name == powerups [i].name) {
						itemStored.GetComponent<Image> ().sprite = item.gameObject.GetComponent<Image> ().sprite;
						if (powerups [i].count >= 1000) {
							item.GetComponent<BehaviourStore> ().locked = true;
						}
						break;
					}
				}
				powerupsStored.Add (itemStored);
			}
		}
	}


	public void StartTeamsItems ()
	{
		if (GameControl.userInfo.teams != null) {
			GameObject[] items = GameObject.FindGameObjectsWithTag ("Team");
			List<string> teams = GameControl.userInfo.teams;
			int j = 0;
			for (int i = 0; i < teams.Count; i++) {
				GameObject itemStored = GameObject.Instantiate (itemStoredPrefab);
				Rect itemRect = itemStored.GetComponent<Rect> ();
				j = (int)i / cols;
				itemRect.position = new Vector2 (i * 33, -47 * j);
				itemStored.GetComponentInChildren<Text> ().text = teams [i];
				foreach (GameObject item in items) {
					if (item.name.IndexOf (teams [i]) != -1) {
						item.GetComponent<BehaviourStore> ().locked = true;
						break;
					}
				}
				teamsStored.Add (itemStored);
			}
		}
	}

	public void SetTeamsPanel ()
	{
		teamsPanels.SetActive (true);
		SetPortrait (teamsPanels.GetComponentInChildren<Button> ());
		SetTeamsStored ();
	}

	public void SetPowerUpsPanel ()
	{
		powerupsPanel.SetActive (true);
		SetPortrait (powerupsPanel.GetComponentInChildren<Button> ());
		SetPowerUpsStored ();
	}

	public void SetPortrait (Button item)
	{
		portraitPanel.SetActive (true);
		portraitPanel.GetComponentInChildren<Image> ().sprite = item.GetComponent<Image> ().sprite;
		portraitPanel.GetComponentInChildren<Text> ().text = item.GetComponentInChildren<Text> ().text;
	}

	public void SetTeamsStored ()
	{
		foreach (GameObject team in teamsStored) {
			team.transform.parent = mainPanel.transform;
		}
		mainPanel.SetActive (true);
	}

	public void SetPowerUpsStored ()
	{
		foreach (GameObject powerup in powerupsStored) {
			powerup.transform.parent = mainPanel.transform;
		}
		mainPanel.SetActive (true);
	}

	public void HideTeamsStored ()
	{
		foreach (GameObject team in teamsStored) {
			team.transform.parent = null;
		}
		mainPanel.SetActive (false);
	}

	public void HidePowerUpsStored ()
	{
		foreach (GameObject powerup in powerupsStored) {
			powerup.transform.parent = null;
		}
		mainPanel.SetActive (false);
	}

	public void HideTeamPortraitPanel ()
	{
		portraitPanel.SetActive (false);
		teamAttributePanel.SetActive (false);
		HideTeamsStored ();
	}

	public void HidePowerUpPortraitPanel ()
	{
		portraitPanel.SetActive (false);
		powerupAttributePanel.SetActive (false);
		HidePowerUpsStored ();
	}

	public void HideTeamsPanels ()
	{
		teamsPanels.SetActive (false);
		HideTeamPortraitPanel ();
	}

	public void HidePowerUpsPanels ()
	{
		powerupsPanel.SetActive (false);
		HidePowerUpPortraitPanel ();
	}

	public void ExitStore ()
	{
		SceneManager.LoadScene ("Start");
	}
}
