﻿using UnityEngine;
using System.Collections;

public class PlayerGol : MonoBehaviour {
	public GameControl gameControl;
	public int iarch;

	// Use this for initialization
	void Awake () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject == gameControl.tball.gameObject) {
			gameControl.setGol(iarch);
		}
	}

	public void setSecondTime(){
		iarch = (iarch == 0) ? 1:0;
	}
}
