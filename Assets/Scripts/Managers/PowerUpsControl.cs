﻿using UnityEngine;
using System.Collections;

public class PowerUpsControl : MonoBehaviour {

	public delegate void EnableRayCastLayer (int isfor, int raycast);

	public static event EnableRayCastLayer enableRayCast;

	public static PowerUpsControl p;

	public Shader invisibility;

	public Shader normal;

	public Color defaultColor;
		
	void Awake () {
		p = this;
		invisibility = Shader.Find("Transparent/Diffuse");
		normal = Shader.Find("Diffuse");
		defaultColor = new Color (0.75f, 0.75f, 0.75f, 1);
	}

	public void EnableRayCast(int isfor, int raycast){
		enableRayCast(isfor,raycast);
	}
}
