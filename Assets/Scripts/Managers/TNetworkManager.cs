﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class TNetworkManager : MonoBehaviour
{
	public static TNetworkManager n;
	public NetworkManager net;
	public NetworkDiscovery dis;
	public bool isHost = false;

	void Awake ()
	{
		n = this;
		GetNetworkComponents();
	}

	public void GetNetworkComponents(){
		net = GetComponent<NetworkManager> ();
		dis = GetComponent<NetworkDiscovery> ();
	}

	void Update(){
		if (n == null){
			n = this;
			GetNetworkComponents();
		}
	}
}
