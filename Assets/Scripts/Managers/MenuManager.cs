﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;
using System.Collections;

public class MenuManager : MonoBehaviour
{
	public GameObject popUpMessage;

	public Image backgroundPanel;
	public Button startButton;
	public Button playersButton;
	public Button teamsButton;
	public Button stadiumsButton;
	public Button configureButton;

	public Text mynick;
	public Text mymail;
	public Text myteam;
	public Text powerup;
	public Text mystadium;
	public Text mylevel;
	public Text mywins;
	public Text myloses;
	public Text mycoins;
	public Text help;

	public Slider expBar;

	public TowerControl towerControl;

	// Use this for initialization
	void Start ()
	{
		help.text = "";
		int i = Mathf.RoundToInt (UnityEngine.Random.Range (1, 4));
		Sprite background = Resources.Load<Sprite> ("Sprites/Background0" + i);
		backgroundPanel.sprite = background;
		popUpMessage.SetActive (false);
		if (GameControl.userInfo == null)
			GameControl.LoadGame ();
		if (GameControl.userInfo.appid == null || GameControl.userInfo.appid.Length == 0) {
			towerControl.GetAppId ();
		} else {
			if (GameControl.userInfo.srml == null || GameControl.userInfo.srml.Length == 0) {
				
				help.text = "Please register for online match!";
			} else {
				mymail.text = GameControl.userInfo.srml; 
			}
			if (GameControl.userInfo.srnck == null || GameControl.userInfo.srnck.Length == 0) {
				help.text = "Please register for online match!";
			} else {
				mynick.text = GameControl.userInfo.srnck;
			}
			if (GameControl.userInfo.registered == 2) {
				EnableButtons (true);
			} else if (GameControl.userInfo.registered == 1) {
				help.text = "Please confirm your email!";
				towerControl.Register ();
			} else {
				help.text = "Please register for online match!";
			}
			StartLoadGame ();
		}

	}

	void StartLoadGame ()
	{

		if (GameControl.userInfo.team != null) {
			myteam.text = "Team: " + GameControl.userInfo.team.name;
			powerup.text = "PowerUp: " + GameControl.userInfo.powerups[GameControl.userInfo.team.ipower].name;
		} else {
			playersButton.interactable = startButton.interactable = false;
			SceneManager.LoadScene (3);
		}
		//GameControl.userInfo.level = 99;
		mystadium.text = "Stadium: " + GameProperties.stadiums [GameControl.userInfo.istadium];
		mylevel.text = "Level: " + GameControl.userInfo.level;
		mywins.text = "Wins: " + GameControl.userInfo.wins;
		myloses.text = "Loses: " + GameControl.userInfo.loses;
		mycoins.text = "Coins: " + GameControl.userInfo.coins;
		expBar.value = GameControl.userInfo.exp;
	}

	void OnEnable ()
	{
		TowerControl.OnGetAppId += AppIdResponse;
		TowerControl.OnRegister += RegisterResponse;
		TowerControl.OnCreateGame += CreateGameResponse;
	}

	void OnDisable ()
	{
		TowerControl.OnGetAppId -= AppIdResponse;
		TowerControl.OnRegister -= RegisterResponse;
		TowerControl.OnCreateGame -= CreateGameResponse;
	}

	public void Register ()
	{
		if (GameControl.userInfo.srml.Length == 0 || GameControl.userInfo.srml.IndexOf ("@") == -1 || GameControl.userInfo.srnck.Length == 0 || GameControl.userInfo.registered != 2) {
			SceneManager.LoadScene ("Register");
		}
	}

	public void RegisterResponse (string response, long responseCode)
	{
		try {
			GameControl.userInfo.registered =	Convert.ToInt16 (response);
			/*PlayerPrefs.SetString ("srml", GameProperties.srml);
			PlayerPrefs.SetString ("srnck", GameProperties.srnck);
			PlayerPrefs.SetInt ("registered", GameProperties.registered);
			PlayerPrefs.Save ();*/
			GameControl.SaveGame ();
			if (GameControl.userInfo.registered == 2) {
				EnableButtons (true);
			}
		} catch (FormatException ex) {
		}
	}

	public void AppIdResponse (string response, long responseCode)
	{
		if (response != "F") {
			GameControl.userInfo.appid = response;
			GameControl.SaveGame ();
			if (GameControl.userInfo.registered == 0) {
				SceneManager.LoadScene (1);
			} else {
				StartLoadGame ();
			}
		}
	}

	public void PlayersMenu ()
	{
		SceneManager.LoadScene ("Players");
	}

	public void TeamsMenu ()
	{
		SceneManager.LoadScene ("Teams");
	}

	public void StadiumsMenu ()
	{
		SceneManager.LoadScene ("Stadiums");
	}

	public void ConfigureMenu ()
	{
		SceneManager.LoadScene ("Configure");
	}

	public void StoreMenu(){
		SceneManager.LoadScene("Store");
	}

	public void GameStart ()
	{
		GameControl.gmi = -1;
		TNetworkManager.n.isHost = true;
		TNetworkManager.n.net.networkAddress = "localhost";
		TNetworkManager.n.net.onlineScene = GameProperties.stadiums [GameControl.userInfo.istadium];
		TNetworkManager.n.net.StartHost ();
	}

	public void CreateGameResponse (string response, long responseCode)
	{
		if (responseCode == 200) {
			if (response.IndexOf ("gmi:") != -1) {
				try {
					GameControl.gmi = Convert.ToInt32 (response.Replace ("gmi:", ""));
					SceneManager.LoadScene (GameControl.userInfo.istadium + 6);
				} catch (FormatException ex) {
				}
			}
		} else {
			Text[] message = popUpMessage.GetComponentsInChildren<Text> ();
			message [0].text = "We dont have connection with the server, you want to start a computer match?";
			popUpMessage.SetActive (true);
		}
	}

	public void Accept ()
	{
		GameControl.gmi = -1;
		SceneManager.LoadScene (GameControl.userInfo.istadium + 6);
	}

	public void Cancel ()
	{
		popUpMessage.SetActive (false);
	}

	public void Exit ()
	{
		Application.Quit ();
	}

	void EnableButtons (bool enabled)
	{
		playersButton.interactable = enabled;
		configureButton.interactable = !enabled;
	}
}
