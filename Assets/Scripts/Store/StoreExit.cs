﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StoreExit : MonoBehaviour {

	public GameObject exitPanel;

	void OnMouseDown(){
		if (!UIManager.ui.uilocked){
			UIManager.ui.uilocked = true;
			exitPanel.SetActive(true);
		}
	}

	public void HideExitPanel(){
		UIManager.ui.uilocked = false;
		exitPanel.SetActive(false);
	}
}
