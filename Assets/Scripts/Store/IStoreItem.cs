﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class IStoreItem: MonoBehaviour {

	public Image portraitImage;
	public Text portraitText;
	public GameObject buyButton;
	public string nameItem;
	public float price;
	public bool locked;
	public abstract void SetAttributes(Button item, bool locked);
	public abstract void ItemConfirm();

	void Start(){
		buyButton.GetComponent<Button>().onClick.AddListener( () => ItemConfirm());
	}
}
