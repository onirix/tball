﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoreItemPowerUp : IStoreItem
{
	public Text descriptionText;
	public Text priceText;
	string nameItem;
	public override void SetAttributes (Button item, bool locked)
	{
		Text[] attributes = item.GetComponentsInChildren<Text>();

		IPowerUp powerup = item.gameObject.GetComponent (typeof(IPowerUp)) as IPowerUp;
		nameItem = attributes[0].text;
		descriptionText.text = attributes[1].text;
		price = powerup.Threshold;
		priceText.text = "" + price;
		this.locked = locked;
		buyButton.SetActive (!locked);
		portraitImage.sprite = item.GetComponent<Image>().sprite;
		portraitText.text = nameItem;
	}

	public override void ItemConfirm ()
	{
		if (!locked && GameControl.userInfo.coins >= price) {
			if (GameControl.userInfo.powerups != null) {
				List<PowerUpsStored> powerups = GameControl.userInfo.powerups;
				for (int i = 0; i < powerups.Count; i++) {
					PowerUpsStored powerup = powerups [i];
					if (powerup.name == nameItem) {
						powerup.count++;
						return;
					}
				}
			}else{
				GameControl.userInfo.powerups = new List<PowerUpsStored>();
			}
			GameControl.userInfo.powerups.Add (new PowerUpsStored (nameItem, 1));
		}
	}
}
