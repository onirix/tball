﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoreItemTeam : IStoreItem
{
	public Slider attackBar;
	public Slider defenseBar;
	public Slider aereaBar;
	public Text powerUpText;
	public Text priceText;

	public override void SetAttributes (Button item, bool locked)
	{
		Text[] attributes = item.GetComponentsInChildren<Text> ();

		ITeam team = item.gameObject.GetComponent (typeof(ITeam)) as ITeam;
		price = (team.bouncy + team.aerea) * 1000;
		attackBar.value = team.bouncy; 
		defenseBar.value = 1 - team.bouncy;
		aereaBar.value = team.aerea;
		nameItem = attributes [0].text;
		powerUpText.text = attributes [1].text;
		priceText.text = "" + price;
		this.locked = locked;
		buyButton.SetActive (!locked);
		portraitImage.sprite = item.GetComponent<Image>().sprite;
		portraitText.text = nameItem;
	}

	public override void ItemConfirm ()
	{
		if (!locked && GameControl.userInfo.coins >= price) {
			if (GameControl.userInfo.teams == null) {
				GameControl.userInfo.teams = new List<string> ();
			}
			GameControl.userInfo.teams.Add (nameItem);
		}
	}
}
