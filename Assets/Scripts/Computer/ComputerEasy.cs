﻿using UnityEngine;
using System.Collections;

public class ComputerEasy : IComputer
{
	
	public ComputerEasy(){
		level = 90;
	}

	public override void HitBall (Rigidbody ball)
	{
		/*int mask = 0;
		NavMeshPath path =  new NavMeshPath();
		NavMesh.CalculatePath(ball.position, target,mask,path);*/
		Vector3 direction = (target - ball.position).normalized * 10.0f * (UnityEngine.Random.value + 1.0f);
//		Debug.Log ("computer direction hit: " + direction);
		ball.AddForce (direction, ForceMode.Impulse);
		GameNetworkControl.gn.ballhit = 2;
	}

	public override void SetPowerUp ()
	{
		int i;
		if ((powerup.ForAll & 3) == 3) {
			i = UnityEngine.Random.Range (1, 3);
		} else {
			i = powerup.ForAll ^ 3;
		}
		BehaviorPlayer playerBehavior = GameObject.Find ("player_" + i + UnityEngine.Random.Range (1, 12)).GetComponent<BehaviorPlayer> ();
		playerBehavior.SetPowerUp (powerup, true);
	}

	public override void LoadPowerUp(IPowerUp powerup){
		this.powerup = powerup;
		this.powerup.Armed = false;
		this.threshold = powerup.Threshold - level;
	}
}
