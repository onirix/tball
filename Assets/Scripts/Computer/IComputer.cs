﻿using UnityEngine;
using System.Collections;

public abstract class IComputer
{
	public IPowerUp powerup;

	public Vector3 target;

	public int rebounds;

	public int level;

	public int threshold;

	public abstract void HitBall (Rigidbody ball);

	public abstract void SetPowerUp ();

	public abstract void LoadPowerUp(IPowerUp powerup);
}
