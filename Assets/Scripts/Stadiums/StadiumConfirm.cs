﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class StadiumConfirm : MonoBehaviour
{
	public GameObject portraitPanel;
	public Text stadium;

	void Start ()
	{
	}

	public void StadiumSelectedConfirm ()
	{
		for (int i = 0; i < GameProperties.stadiums.Length; i++) {
			if (stadium.text == GameProperties.stadiums [i]) {
				GameControl.userInfo.istadium = i;
				GameControl.SaveGame ();
				SceneManager.LoadScene ("Start");
			}
		}
	}

	public void HidePortraitPanel ()
	{
		UIManager.ui.uilocked = false;
		portraitPanel.SetActive (false);
	}
}
