﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class StadiumSelected : MonoBehaviour {
	public GameObject portrait;
	public Image portraitImage;
	public Text portraitText;

	public Sprite uistadium;

	void OnMouseDown ()
	{
		if (!UIManager.ui.uilocked){
			portraitImage.sprite = uistadium;
			portraitText.text = name;
			portrait.SetActive(true);
		}
	}
}
