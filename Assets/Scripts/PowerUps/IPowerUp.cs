﻿using UnityEngine;
using System.Collections;

public interface IPowerUp
{
	Transform PowerupObject {
		get;
	}

	Transform ReferenceObject {
		get;
	}

	int ForAll {
		get;
	}

	int Threshold {
		get;
	}

	int Mask {
		get;
	}

	float Timer {
		get;
	}

	bool Armed {
		get;
		set;
	}

	void Activate (Rigidbody ball);

	void StartPowerUp (Transform local, bool computer);

	void UpdatePowerUp ();

	void MouseButtonDown (RaycastHit rayHit);

	void MouseButtonUp (RaycastHit rayHit);

	void MouseButtonPosition (RaycastHit rayHit);

	void PowerUpBallEffect(NetUser user, int mode);
}
