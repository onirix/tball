﻿using UnityEngine;
using System.Collections;

public class PowerUpExplode : MonoBehaviour, IPowerUp
{
	public delegate void BallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position);

	public event BallEffect ballEffect;

	public Transform numbers;

	private bool armed;

	private int forall = 2;

	private int threshold = 140;

	private int mask;

	private float timer;

	private Transform localObject;

	private Transform reference;

	private TextMesh cnumber;

	private Renderer renderer;

	public bool Armed {
		get {
			return armed;
		}
		set {
			//forall = 2;
			//threshold = 40;
			armed = value;
		}
	}

	public int ForAll {
		get {
			return forall;
		}
	}

	public int Threshold {
		get {
			return threshold;
		}
	}

	public int Mask {
		get {
			return mask;
		}
	}

	public float Timer {
		get {
			return mask;
		}
	}

	public Transform PowerupObject {
		get {
			return localObject;
		}
	}

	public Transform ReferenceObject {
		get {
			return reference;
		}
	}

	public void Activate (Rigidbody ball)
	{
		/*Vector3 forceExplosion = new Vector3 (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
		ball.AddForce (forceExplosion, ForceMode.Impulse);
		if (localObject != null) {
			Destroy (localObject.gameObject);
		}
		armed = false;
		Destroy (reference.gameObject);*/
	}

	public void StartPowerUp (Transform local, bool computer)
	{
		GameControl.g.SendPowerUpMessage (-1);
		PowerUpsControl.p.EnableRayCast (forall, 2);
		reference = local;
		armed = true;
		mask = LayerMask.GetMask ("Floor");
		reference.GetComponentInChildren<Renderer> ().material.color = Color.red;

		localObject = Instantiate (numbers, new Vector3 (local.position.x, 3.0f, local.position.z), local.rotation) as Transform;
		localObject.Rotate (30, 90, 0);
		cnumber = localObject.GetComponent<TextMesh> ();
		cnumber.alignment = TextAlignment.Center;
		cnumber.anchor = TextAnchor.MiddleCenter;
		cnumber.fontStyle = FontStyle.Bold;
		cnumber.color = Color.red;
		cnumber.offsetZ = 0.5f;
		cnumber.text = "1";

		renderer = reference.gameObject.GetComponentInChildren<Renderer> ();
	}

	public void UpdatePowerUp ()
	{
		timer += Time.deltaTime;
		if (timer > 60) {
			if (localObject != null) {
				Vector3 forceExplosion = new Vector3 (UnityEngine.Random.value, 1.0f, UnityEngine.Random.value);
				Destroy (localObject.gameObject);
				//Destroy (reference.gameObject);
				Rigidbody body = reference.GetComponentInChildren<Rigidbody> ();
				body.isKinematic = false;
				body.AddForce (forceExplosion * 30.0f, ForceMode.Impulse);
				body.angularVelocity += forceExplosion;
			} else {
				if (timer > 70) {
					armed = false;
					timer = 0;
					Destroy (reference.gameObject);
				}
			}
		} else {
			cnumber.text = "" + (60 - Mathf.Round (timer));
			float greenblue = 0.75f - timer * 1.25f / 100.0f;
			float red = 0.75f + timer * 0.416f / 100f;
			if (greenblue < 0.0f) {
				greenblue = 0.0f;
			}
			if (red > 1.0f) {
				red = 1.0f;
			}
			renderer.material.color = new Color (red, greenblue, greenblue, 1.0f);
		}
	}

	public void MouseButtonDown (RaycastHit rayHit)
	{
	}

	public void MouseButtonUp (RaycastHit rayHit)
	{
	}

	public void MouseButtonPosition (RaycastHit rayHit)
	{
	}

	public void PowerUpBallEffect (NetUser user, int mode)
	{
		if (mode == 0){
			ballEffect += user.PowerUpBallEffect;
		}else {
			ballEffect -= user.PowerUpBallEffect;
		}
	}
}
