﻿using UnityEngine;
using System.Collections;

public class PowerUpDirection : MonoBehaviour, IPowerUp
{

	public delegate void BallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position);

	public event BallEffect ballEffect;

	public Transform arrow;

	private bool armed;

	private int forall = 1;

	private int threshold = 110;

	private int mask;

	private float timer;

	private Transform localObject;

	private Transform reference;

	public bool Armed {
		get {
			return armed;
		}
		set {
			//forall = 1;
			//threshold = 10;
			armed = value;
		}
	}

	public int ForAll {
		get {
			return forall;
		}
	}

	public int Threshold {
		get {
			return threshold;
		}
	}

	public int Mask {
		get {
			return mask;
		}
	}

	public float Timer {
		get {
			return mask;
		}
	}

	public Transform PowerupObject {
		get {
			return localObject;
		}
	}

	public Transform ReferenceObject {
		get {
			return reference;
		}
	}

	public void Activate (Rigidbody ball)
	{
		Vector3 newpos = localObject.position + localObject.forward;
		Vector3 newdir = localObject.forward;
		newdir.y = 0.5f;
		float magnitude = ball.velocity.magnitude;
		if (ballEffect != null)
			ballEffect(newdir * magnitude, Vector3.zero, Vector3.zero, newpos);
		if (localObject != null)
			Destroy (localObject.gameObject);
		armed = false;
		reference.GetComponentInChildren<Renderer> ().material.color = new Color (0.75f, 0.75f, 0.75f, 1.0f);
	}

	public void StartPowerUp (Transform local, bool computer)
	{
		reference = local;
		localObject = Instantiate (arrow, new Vector3 (local.position.x, 0.5f, local.position.z), local.rotation) as Transform;
		localObject.name = "powerup_" + local.name;
		mask = LayerMask.GetMask ("Floor");
		reference.GetComponentInChildren<Renderer> ().material.color = Color.yellow;

		CameraFollow.c.SetTarget (reference.transform);
		if (computer) {
			Vector3 pointTo = GameControl.g.iaComputer.target - local.position;
			SetRotation (pointTo);
			FinishPowerUp ();
		} else {
			GameControl.g.SendPowerUpMessage (1);
		}
	}

	public void UpdatePowerUp ()
	{
	}

	public void MouseButtonDown (RaycastHit rayHit)
	{
		FinishPowerUp ();
	}

	public void MouseButtonUp (RaycastHit rayHit)
	{
	}

	public void MouseButtonPosition (RaycastHit rayHit)
	{
		Vector3 objectToMouse = rayHit.point - localObject.position;

		objectToMouse.y = 0.0f;

		SetRotation (objectToMouse);
		//localObject.Rotate(90,0,0);
		/*float angle = Vector3.Angle (localObject.forward, rayHit.point);
		Debug.Log ("angle: " + angle);
		if (angle > 10) {
			localObject.RotateAround (reference.position, Vector3.up, angle * 0.1f);
		}*/
	}

	private void SetRotation (Vector3 lookAt)
	{

		Quaternion newRotation = Quaternion.LookRotation (lookAt);

		localObject.rotation = newRotation;
	}

	private void FinishPowerUp ()
	{
		GameControl.g.SendPowerUpMessage (-1);
		PowerUpsControl.p.EnableRayCast (forall, 2);
		armed = true;
		CameraFollow.c.SetTarget (GameControl.g.tball);
	}

	public void PowerUpBallEffect (NetUser user, int mode)
	{
		if (mode == 0){
			ballEffect += user.PowerUpBallEffect;
		}else {
			ballEffect -= user.PowerUpBallEffect;
		}
	}
}
