﻿using UnityEngine;
using System.Collections;

public class PowerUpCrystal : MonoBehaviour, IPowerUp
{
	public delegate void BallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position);

	public event BallEffect ballEffect;

	private bool armed;

	private int forall = 3;

	private int threshold = 105;

	private int mask;

	private float timer;

	private Transform localObject;

	private Transform reference;

	public bool Armed {
		get {
			return armed;
		}
		set {
			//forall = 3;
			//threshold = 5;
			armed = value;
		}
	}

	public int ForAll {
		get {
			return forall;
		}
	}

	public int Threshold {
		get {
			return threshold;
		}
	}

	public int Mask {
		get {
			return mask;
		}
	}

	public float Timer {
		get {
			return mask;
		}
	}

	public Transform PowerupObject {
		get {
			return localObject;
		}
	}

	public Transform ReferenceObject {
		get {
			return reference;
		}
	}

	public void Activate (Rigidbody ball)
	{
		//ball.AddForce (-ball.velocity, ForceMode.Impulse);
		if (ballEffect != null)
			ballEffect (-ball.velocity, ball.velocity, ball.angularVelocity, ball.position);
		if (localObject != null)
			Destroy (localObject.gameObject);
		armed = false;
		reference.GetComponentInChildren<Renderer> ().material.color = new Color (0.75f, 0.75f, 0.75f, 1.0f);
	}

	public void StartPowerUp (Transform local, bool computer)
	{
		GameControl.g.SendPowerUpMessage (-1);
		PowerUpsControl.p.EnableRayCast (forall, 2);
		reference = local;
		mask = LayerMask.GetMask ("Floor");
		armed = true;
		reference.GetComponentInChildren<Renderer> ().material.color = Color.white;
	}

	public void UpdatePowerUp ()
	{
	}

	public void MouseButtonDown (RaycastHit rayHit)
	{
	}

	public void MouseButtonUp (RaycastHit rayHit)
	{
	}

	public void MouseButtonPosition (RaycastHit rayHit)
	{
	}

	public void PowerUpBallEffect (NetUser user, int mode)
	{
		if (mode == 0){
			ballEffect += user.PowerUpBallEffect;
		}else {
			ballEffect -= user.PowerUpBallEffect;
		}
	}
}
