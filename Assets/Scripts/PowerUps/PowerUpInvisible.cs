﻿using UnityEngine;
using System.Collections;

public class PowerUpInvisible : MonoBehaviour, IPowerUp
{
	public delegate void BallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position);

	public event BallEffect ballEffect;

	private bool armed;

	private int forall = 3;

	private int threshold = 105;

	private int mask;

	private float timer;

	private Transform localObject;

	private Transform reference;

	//private TextMesh cnumber;

	private Renderer renderer;

	public bool Armed {
		get {
			return armed;
		}
		set {
			//forall = 3;
			//threshold = 5;
			armed = value;
		}
	}

	public int ForAll {
		get {
			return forall;
		}
	}

	public int Threshold {
		get {
			return threshold;
		}
	}

	public int Mask {
		get {
			return mask;
		}
	}

	public float Timer {
		get {
			return mask;
		}
	}

	public Transform PowerupObject {
		get {
			return localObject;
		}
	}

	public Transform ReferenceObject {
		get {
			return reference;
		}
	}

	public void Activate (Rigidbody ball)
	{
	}

	public void StartPowerUp (Transform local, bool computer)
	{
		GameControl.g.SendPowerUpMessage (-1);
		PowerUpsControl.p.EnableRayCast (forall, 2);
		reference = local;
		/*localObject = Instantiate (numbers, new Vector3 (local.position.x, 2.5f, local.position.z), local.rotation) as Transform;
		localObject.Rotate(30,-90,0);
		cnumber = localObject.GetComponent<TextMesh> ();
		cnumber.alignment = TextAlignment.Center;
		cnumber.anchor = TextAnchor.MiddleCenter;
		cnumber.fontStyle = FontStyle.Bold;
		cnumber.offsetZ = 0.5f;
		cnumber.text = "1";*/
		renderer = reference.gameObject.GetComponentInChildren<Renderer> ();
		renderer.material.shader = PowerUpsControl.p.invisibility;
		renderer.material.color = new Color (0.0f, 1.0f, 1.0f, 0.3f);
		reference.gameObject.GetComponentsInChildren<CapsuleCollider> () [1].isTrigger = true;
		armed = true;
		mask = LayerMask.GetMask ("Shootable");
	}

	public void UpdatePowerUp ()
	{
		timer += Time.deltaTime;
		if (timer > 100.0f) {

			armed = false;
			/*if (localObject != null)
				Destroy (localObject.gameObject);*/
			renderer.material.shader = PowerUpsControl.p.normal;
			reference.GetComponentInChildren<Renderer> ().material.color = new Color (0.75f, 0.75f, 0.75f, 1.0f);

			reference.gameObject.GetComponentsInChildren<CapsuleCollider> () [1].isTrigger = false;
			timer = 0;
		} else {
			//cnumber.text = "" + Mathf.Round (timer);
			float alpha = 0.3f;
			float red = timer / 100.0f;
			float greenblue = 1.0f;
			if (timer > 30.0f) {
				alpha = red;
				if (timer > 50.0f) {
					if (timer < 75.0f) {
						greenblue = 1.0f - (timer - 50.0f) / 100.0f;
					} else {
						red = greenblue = 0.75f;
					}
				}
			}
			renderer.material.color = new Color (red, greenblue, greenblue, alpha);
		}
	}

	public void MouseButtonDown (RaycastHit rayHit)
	{
	}

	public void MouseButtonUp (RaycastHit rayHit)
	{
	}

	public void MouseButtonPosition (RaycastHit rayHit)
	{
	}

	public void PowerUpBallEffect (NetUser user, int mode)
	{
		if (mode == 0){
			ballEffect += user.PowerUpBallEffect;
		}else {
			ballEffect -= user.PowerUpBallEffect;
		}
	}
}
