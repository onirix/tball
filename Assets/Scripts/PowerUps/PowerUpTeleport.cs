﻿using UnityEngine;
using System.Collections;

public class PowerUpTeleport : MonoBehaviour, IPowerUp
{
	public delegate void BallEffect (Vector3 force, Vector3 velocity, Vector3 angularVelocity, Vector3 position);

	public event BallEffect ballEffect;

	public Transform portal;

	private bool armed;

	private int forall = 3;

	private int threshold = 110;

	private int mask;

	private float timer;

	private Transform localObject;

	private Transform reference;

	public bool Armed {
		get {
			return armed;
		}
		set {
			//forall = 3;
			//threshold = 10;
			armed = value;
		}
	}

	public int ForAll {
		get {
			return forall;
		}
	}

	public int Threshold {
		get {
			return threshold;
		}
	}

	public int Mask {
		get {
			return mask;
		}
	}

	public float Timer {
		get {
			return mask;
		}
	}

	public Transform PowerupObject {
		get {
			return localObject;
		}
	}

	public Transform ReferenceObject {
		get {
			return reference;
		}
	}

	public void Activate (Rigidbody ball)
	{
		//ball.position = new Vector3 (localObject.transform.position.x, ball.position.y, localObject.transform.position.z);
		if (ballEffect != null)
			ballEffect (Vector3.zero, Vector3.zero, Vector3.zero, new Vector3 (localObject.transform.position.x, ball.position.y, localObject.transform.position.z));
		if (localObject != null)
			Destroy (localObject.gameObject);
		Transform startportal = reference.FindChild (localObject.name + "_ini");
		if (startportal != null)
			Destroy (startportal.gameObject);
		armed = false;
		reference.GetComponentInChildren<Renderer> ().material.color = new Color (0.75f, 0.75f, 0.75f, 1.0f);
	}

	public void StartPowerUp (Transform local, bool computer)
	{
		reference = local;
		localObject = Instantiate (portal, new Vector3 (local.position.x, 0.5f, local.position.z), local.rotation) as Transform;
		localObject.name = "powerup_" + local.name + "_" + UnityEngine.Random.value;

		Transform startportal = Instantiate (portal, new Vector3 (local.position.x, 0.5f, local.position.z), local.rotation) as Transform;
		startportal.parent = reference;
		startportal.name = localObject.name + "_ini";
		startportal.GetComponent<Renderer> ().material.color = localObject.GetComponent<Renderer> ().material.color = new Color (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, 1);
		mask = LayerMask.GetMask ("Shootable");
		reference.GetComponentInChildren<Renderer> ().material.color = Color.white;

		CameraFollow.c.SetTarget (reference.transform);
		if (computer) {
			Transform endPoint = GameObject.Find ("player_2" + UnityEngine.Random.Range (9, 12)).GetComponent<Transform> ();
			localObject.position = new Vector3 (endPoint.position.x, 0.5f, endPoint.position.z);
			FinishPowerUp ();
		} else {
			GameControl.g.SendPowerUpMessage (2);
		}
	}

	public void UpdatePowerUp ()
	{
	}

	public void MouseButtonDown (RaycastHit rayHit)
	{
		Debug.Log (rayHit.collider.tag + ", " + reference.tag);
		if (rayHit.collider.tag == reference.tag) {
			FinishPowerUp ();
		}
	}

	public void MouseButtonUp (RaycastHit rayHit)
	{
	}

	public void MouseButtonPosition (RaycastHit rayHit)
	{
		if (localObject != null)
			localObject.position = new Vector3 (rayHit.collider.transform.position.x, 0.5f, rayHit.collider.transform.position.z);
	}

	private void FinishPowerUp ()
	{
		GameControl.g.SendPowerUpMessage (-1);
		PowerUpsControl.p.EnableRayCast (forall, 2);
		armed = true;
		CameraFollow.c.SetTarget (GameControl.g.tball);
	}

	public void PowerUpBallEffect (NetUser user, int mode)
	{
		if (mode == 0) {
			ballEffect += user.PowerUpBallEffect;
		} else {
			ballEffect -= user.PowerUpBallEffect;
		}
	}

}
